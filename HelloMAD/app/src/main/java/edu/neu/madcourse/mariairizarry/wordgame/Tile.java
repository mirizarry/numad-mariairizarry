package edu.neu.madcourse.mariairizarry.wordgame;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by mirizarry on 2/21/2015.
 */
public class Tile implements Serializable{

    private int TICK = 20000;
    private char letter;
    private int points, id;
    private int tick; // miliseconds
    private int x, y;
    private ArrayList<ArrayList<Integer>> partOf;
    private boolean inAWord;
    private static String letterString = "AAAAAAAAABBCCDDDDEEEEEEEEEEEEFFGGGHHIIIIIIIIIJKLLLLMMNNNNNNOOOOOOOOPPQRRRRRRSSSSTTTTTTUUUUVVWWXYYZ";
    private static int[] pointArr = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10,
                                     1, 1, 1, 1, 4, 2, 8, 4, 10};
    private boolean isConnected;

    Tile(int id)
    {
            this.x = -1;
            this.y = -1;
            this.tick = TICK;
            this.inAWord = false;
            this.id = id;
            this.isConnected = false;
            Random randomGenerator = new Random();
           // StringBuilder sb = new StringBuilder();
            int letterIndex = randomGenerator.nextInt(letterString.length());
            char c = letterString.charAt(letterIndex);
//            for(int i = 0; i < letterString.length(); i++)
//            {
//                if(letterIndex!= i) sb.append(letterString.charAt(i));
//            }
//            letterString = sb.toString();

            this.partOf = new ArrayList<>(4);
            this.letter = c;
            this.points = pointArr[c-65];

    }

    Tile(int id, char letter)
    {
        this.x = -1;
        this.y = -1;
        this.tick = TICK;
        this.inAWord = false;
        this.id = id;
        this.isConnected = false;

        char c = letter;

        this.partOf = new ArrayList<>(4);
        this.letter = c;
        this.points = pointArr[c-65];
    }

    public int getID (){
        return this.id;
    }

    public char getLetter()
    {
        return this.letter;
    }

    public int getPoints()
    {
        return this.points;
    }

    public void tick()
    {
        this.tick -= 100;
    }
    public void decrementTick(int number){
        this.tick = this.tick - number;
    }

    public int getTick()
    {
        return this.tick;
    }

    public boolean expired()
    {
        return this.tick <= 0;
    }

    public static boolean outOfTiles()
    {
        return letterString.length()==0;
    }

    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }

    public void setX(int x)
    {
       this.x = x;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public boolean isConnected(){
        return this.isConnected;
    }

    public void setConnected(boolean b){
        this.isConnected = b;
    }
    public void addWord(ArrayList<Tile> word)
    {
        ArrayList<Integer> w = new ArrayList<>();

        for(int i = 0; i < word.size(); i++){
            w.add(word.get(i).getID());
        }
        this.partOf.add(w);
        this.inAWord = true;
    }

    public boolean isInAWord()
    {
        return this.inAWord;
    }

    public void setInWordFlag(boolean b)
    {
        this.inAWord = b;
    }

    public void resetTick(){
        this.tick = TICK;
    }
    public void reset(HashMap<Integer, Tile> placed)
    {
        for(int i = 0; i < partOf.size(); i++) {
            ArrayList<Integer> ids = partOf.get(i);
            for(int j = 0; j < ids.size(); j++)
            {
                Integer id = ids.get(j);
                Tile t = placed.get(id);
                t.resetTick();
            }

        }
    }

    public boolean abandoned(HashMap<Integer, Tile> tiles){

        if(!this.inAWord) return false;
        for(int i = 0; i < partOf.size(); i++){
            ArrayList<Integer> word = partOf.get(i);
            for(int j = 0; j < word.size(); j++){
                Integer id = word.get(j);
                if(id==this.id) continue;
                if(tiles.containsKey(id))
                    return false;
            }
        }
        return true;
    }


}
