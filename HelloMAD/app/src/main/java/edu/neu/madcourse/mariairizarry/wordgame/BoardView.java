package edu.neu.madcourse.mariairizarry.wordgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import edu.neu.madcourse.mariairizarry.R;

public class BoardView extends View {

    private static final String Tag = "WordFade";

    protected static final String SELX = "selX";
    protected static final String SELY = "selY";
    protected static final String VIEW_STATE = "viewState";
    protected static final int ID = 42;

    protected float width;    // width of one tile
    protected float height;   // height of one tile
    protected int selX;       // X index of selection
    protected int selY;       // Y index of selection
    protected final Rect selRect = new Rect();
    protected final Rect selPiece = new Rect();
    protected boolean selPiecePlaced = false;
    protected boolean selRectPlaced = false;

    private Gameplay game;

    public BoardView(Context context) {
        super(context);
        this.game = (Gameplay) context;
        setFocusable(true);
        setFocusableInTouchMode(true);
      //  setId(ID);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        width = w / 9f;
        height = h / 12f;

        //getRect(selX, selY, selRect);
        super.onSizeChanged(w, h, oldw, oldh);
    }
    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        // Draw background
        Paint background = new Paint();
        background.setColor(getResources().getColor(R.color.puzzle_background));
        canvas.drawRect(0, 0, getWidth(), getHeight(), background);

        Paint hilite = new Paint();
        hilite.setColor(getResources().getColor(R.color.puzzle_hilite));

        Paint light = new Paint();
        light.setColor(getResources().getColor(R.color.puzzle_light));

        Paint rack = new Paint();
        rack.setColor(getResources().getColor(R.color.rack_background));

        Paint placeHolder = new Paint();
        placeHolder.setColor(getResources().getColor(R.color.rack_placeholder));

        Paint holderShadow = new Paint();
        holderShadow.setColor(getResources().getColor(R.color.rack_placeholder_shadow));

        Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
        foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
        foreground.setStyle(Paint.Style.FILL);
        foreground.setTextSize(height * 0.75f);
        foreground.setTextScaleX(width / height);
        foreground.setTextAlign(Paint.Align.CENTER);

        Paint.FontMetrics fm = foreground.getFontMetrics();

        //Draw Grid
        float x = width / 2;
        float y = width / 2 - (fm.ascent + fm.descent) / 2;
        canvas.drawRect(0, 0, 3 * width, height * 12, rack);
        for(int i = 0; i < 12; i++)
        {
            canvas.drawLine(width * 3, i * height, getWidth(), i * height,
                    light);
            canvas.drawLine(width * 3, i * height + 1, getWidth(), i * height + 1,
                    hilite);
        }

        for(int i = 3; i < 9; i++)
        {
            canvas.drawLine(i * width, 0, i * width, getHeight(),
                    light);
            canvas.drawLine(i * width + 1, 0, i * width + 1,
                    getHeight(), hilite);
        }
        // end of drawing grid

        //Draw new tile pieces

        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 7; j++)
            {

                canvas.drawRect((width * i) + 2, (height * j) + 2, (width * (i + 1)) - 4, (height * (j + 1)) - 4, holderShadow);
                canvas.drawRect((width * i) + 3, (height * j) + 3, (width * (i + 1)) - 6, (height * (j + 1)) - 6, placeHolder);

                if(this.game.rackPositionAvailable(i,j)) {
                    for (int k = 0; k < this.game.getTiles().size(); k++) {

                        Tile tileToDraw = this.game.getTile(k);

                        if ((tileToDraw.getX() == -1 && tileToDraw.getY() == -1)) {

                               tileToDraw.setX(i);
                               tileToDraw.setY(j);
                               break;
                        }

                    }
                }

            }
        }

        for(int i = 0; i < this.game.getTiles().size(); i++)
        {
            Tile tileToDraw = this.game.getTile(i);
            String letterToDraw = this.game.getTileLetter(i);

            canvas.drawRect((width * tileToDraw.getX()) + 3, (height * tileToDraw.getY()) + 3, (width * (tileToDraw.getX() + 1)) - 6, (height * (tileToDraw.getY() + 1)) - 6, hilite);
            canvas.drawText(letterToDraw, tileToDraw.getX() * width + x, tileToDraw.getY() * height + y, foreground);
        }


        for(int i = 3; i < 9; i++)
        {
            for(int j = 0; j < 12; j++)
            {
                Tile tileToDraw = this.game.getTile(i-3, j);
                Paint tilePaint = new Paint();
                Paint letterColor = foreground;
                letterColor.setAlpha(255);
                tilePaint.setColor(Color.WHITE);
                if(tileToDraw!=null) {
                    String letterToDraw = this.game.getTileLetter(i-3, j);
                    if(tileToDraw.isInAWord())
                    {
                        int tick = tileToDraw.getTick();

                        tilePaint.setColor(Color.YELLOW);
                        if(tick < 5000) tilePaint.setColor(Color.RED);
                        tilePaint.setAlpha((int) ((tick/200) * 2.55));
                        letterColor.setAlpha((int) ((tick/200) * 2.55));


                    }

                    canvas.drawRect((width * i) + 3, (height * j) + 3, (width * (i + 1)) - 2, (height * (j + 1)) - 2, tilePaint);

                    canvas.drawText(letterToDraw, i * width + x, j * height + y, letterColor);
                }
            }
        }

        Bitmap b;
        //Draw icons
        if(this.game.canSplit())
        {
            Paint iconPaint = new Paint();
            iconPaint.setColor(Color.GREEN);


            b = BitmapFactory.decodeResource(getResources(), R.drawable.split);
            canvas.drawBitmap(b, (int) width * 2 - 10, (int) (height * 10), iconPaint);

            b.recycle();
        }

        if(this.game.canDump())
        {
            b = BitmapFactory.decodeResource(getResources(), R.drawable.dump);
            Paint iconPaint = new Paint();
            iconPaint.setColor(Color.GREEN);

            //canvas.drawBitmap(b, (int) width - 5, (int) (height * 7) + 5, iconPaint);
            canvas.drawBitmap(b, (int) 0, (int) (height * 10) + 5, iconPaint);
            b.recycle();
        }

        b = BitmapFactory.decodeResource(getResources(), R.drawable.up);
        canvas.drawBitmap(b, (int) width - 5, (int) (height * 7) + 5, hilite);
        b.recycle();

        b = BitmapFactory.decodeResource(getResources(), R.drawable.down);
        canvas.drawBitmap(b, (int) width - 5, (int) (height * 9) + 5, hilite);
        b.recycle();

        b = BitmapFactory.decodeResource(getResources(), R.drawable.left);
        canvas.drawBitmap(b, (int) 0, (int) (height * 8) + 5, hilite);
        b.recycle();

        b = BitmapFactory.decodeResource(getResources(), R.drawable.right);
        canvas.drawBitmap(b, (int) (width * 2 - 10), (int) (height * 8) + 5, hilite);
        b.recycle();

        b = BitmapFactory.decodeResource(getResources(), R.drawable.pause);
        canvas.drawBitmap(b, (int) width - 1, (int) (height * 10) + 7, hilite);
        b.recycle();

        b = BitmapFactory.decodeResource(getResources(), R.drawable.exit);
        canvas.drawBitmap(b, (int) width * 2 - 10, (int) (height * 11), hilite);
        b.recycle();

        b = BitmapFactory.decodeResource(getResources(), R.drawable.mute);
        canvas.drawBitmap(b, (int) 0, (int) (height * 9) + 5, hilite);
        b.recycle();

        //Draw selection rect
        Paint selected = new Paint();
        selected.setColor(getResources().getColor(R.color.puzzle_selected));
        canvas.drawRect(selRect, selected);
        canvas.drawRect(selPiece, selected);

        //draw score text
        foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
        foreground.setColor(Color.WHITE);
        foreground.setStyle(Paint.Style.FILL);
        foreground.setTextSize(height * 0.40f);
        foreground.setTextScaleX(width / height);
        canvas.drawText("Score: "+this.game.getScore(), 2, height * 11 + y, foreground);

        super.onDraw(canvas);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float eventx = event.getX();
        float eventy = event.getY();
        int action = event.getAction();

        if(action != MotionEvent.ACTION_DOWN)
        {
            //touch on grid
            if(selection(event.getX(), event.getY())){
                    if(selPiecePlaced)
                    {
                        if(event.getX() > width * 3) {
                            selRectPlaced = true;
                            select((int) (event.getX() / width),
                                    (int) (event.getY() / height), selRect);

                        }
                        else
                        {
                            select((int) (event.getX() / width),
                                    (int) (event.getY() / height), selPiece);
                        }
                    }
                    else {
                        selPiecePlaced = true;
                        selRectPlaced = false;
                        select((int) (event.getX() / width),
                                (int) (event.getY() / height), selPiece);

                    }
                }

            //touch on rack
            else {
                if (eventy < 7 * height)
                    select((int) (eventx / width),
                            (int) (eventy / height), selPiece);

                else if (splitButton(eventx, eventy) && this.game.canSplit()) {
                    invalidate();
                    this.game.addTileToRack();
                    invalidate();
                } else if (dumpButton(eventx, eventy) && this.game.canDump()) {
                    invalidate();
                    this.game.dumpTile((int) Math.ceil((selPiece.left - 1) / width), (int) Math.ceil((selPiece.top - 1) / height));
                    invalidate();
                } else if (upButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveUp();
                    invalidate();
                } else if (downButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveDown();
                    invalidate();
                } else if (rightButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveRight();
                    invalidate();
                } else if (leftButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveLeft();
                    invalidate();
                } else if (pauseButton(eventx, eventy)){
                    this.game.pause();
                }
                else if(exitButton(eventx, eventy))
                {
                    this.game.exit();
                }
                else if(muteButton(eventx, eventy))
                {
                    this.game.toggleMusic();
                }



            }
        }
        return true;
    }

    public boolean selection(float eventx, float eventy){
        return (eventy < height * 7) || (eventx > width * 3);
    }

    public boolean splitButton(float eventx, float eventy )
    {
        return (eventy > 10 * height) && (eventy < 11 * height) && (eventx > 2 * width);
    }

    public boolean dumpButton(float eventx, float eventy)
    {
       // return (eventy > 7 * height) && (eventy < 8 * height) && (eventx < 2 * width) && (eventx > width);
        return (eventy > 10 * height) && (eventy < 11 * height) && (eventx < width);
    }

    public boolean upButton(float eventx, float eventy)
    {
        return (eventy > 7 * height) && (eventy < 8 * height) && (eventx < 2 * width) && (eventx > width);
    }

    public boolean muteButton(float eventx, float eventy)
    {
        return (eventy > 9 * height) && (eventy < 10 * height) && (eventx < width);
    }

    public boolean downButton(float eventx, float eventy)
    {
        return (eventy > 9 * height) && (eventy < 10 * height) && (eventx < 2 * width) && (eventx > width);
    }

    public boolean rightButton(float eventx, float eventy )
    {
        return (eventy > 8 * height) && (eventy < 9 * height) && (eventx > 2 * width);
    }

    public boolean leftButton(float eventx, float eventy )
    {
        return (eventy > 8 * height) && (eventy < 9 * height) && (eventx < width);
    }

    public boolean exitButton(float eventx, float eventy )
    {
        return (eventy > 11 * height) && (eventx > 2 * width);
    }

    public boolean pauseButton(float eventx, float eventy)
    {
        return (eventy > 10 * height) && (eventy < 11 * height) && (eventx < 2 * width) && (eventx > width);
    }
    public void select(int x, int y, Rect rect) {
        invalidate(rect);

        if(rect == selRect) {
            selX = Math.min(Math.max(x, 3), 8);
            selY = Math.min(Math.max(y, 0), 11);
            invalidate();
            this.game.placeTile( (int) Math.ceil((selPiece.left-1)/width) , (int) Math.ceil((selPiece.top-1)/height), selX-3, selY);
            selPiece.setEmpty();
            selPiecePlaced = false;
            invalidate();
        }

        else if (rect == selPiece)
        {

            selRect.setEmpty();
            selRectPlaced = false;
            invalidate(selRect);
            selX = Math.min(Math.max(x, 0), 8);
            selY = Math.min(Math.max(y, 0), 11);
//            int rectx = (int) Math.ceil((selPiece.left-1)/width);
//            int recty = (int) Math.ceil((selPiece.top-1)/height));
            Log.d("Selector:", "Selected rect is at (" + selX + ", " + selY + ")");
            if(!this.game.containsTile( selX , selY)){
                selPiecePlaced = false;
                Log.d("Selector:", "Selected rect is at (" + selX + ", " + selY + ") and does not contain a tile");
            }
            else{
                SoundEffect letterPicked = new SoundEffect(R.raw.letterpicked);
                letterPicked.play(this.game);

            }

            Log.d("Selector:", "Selected rect is at (" + selX + ", " + selY + ")");

        }
        getRect(selX, selY, rect);
        invalidate(selRect);
        invalidate(rect);
    }

    public void getRect (int x, int y, Rect rect)
    {
        rect.set((int) (x * width) + 1, (int) (y * height) + 1, (int) (x
                * width + width), (int) (y * height + height));
         }



}
