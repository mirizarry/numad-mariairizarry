package edu.neu.madcourse.mariairizarry.wordgame;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import edu.neu.madcourse.mariairizarry.*;

public class WordFade extends Activity {

    private ProgressDialog progressDialog;
    private Context wordFadeContext;
    private Button resume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_fade);
        wordFadeContext = this;
        resume = (Button) findViewById(R.id.resume_game);
       // new LoadBloom().execute();
        resume.setEnabled(false);
        if(InternalStorage.readObject(this, "savedGame") != null) {
            Log.d("savedGame", "not NULL!!!");
            Boolean saved = (Boolean) InternalStorage.readObject(this, "savedGame");
            if (saved)
                resume.setEnabled(true);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("I have resumed", "HAHA");
        Boolean bool = (Boolean) InternalStorage.readObject(this, "savedGame");

        if(InternalStorage.readObject(this, "savedGame") != null) {
            Log.d("savedGame", "not NULL!!!");
            Log.d("bool has a value of", "" + bool);
            Boolean saved = (Boolean) InternalStorage.readObject(this, "savedGame");
            if (saved)
                resume.setEnabled(true);
        }
    }

    public void onClick (View view)
    {
        switch (view.getId())
        {
            case R.id.new_game:
                Intent newGameIntent = new Intent(this, Gameplay.class);
                newGameIntent.putExtra("status", "newGame");
                startActivity(newGameIntent);
                break;
            case R.id.resume_game:
                Intent resumeGameIntent = new Intent(this, Gameplay.class);
                resumeGameIntent.putExtra("status", "resume");
                startActivity(resumeGameIntent);
                break;

            case R.id.acknowledgements:
                Dialog ackInfo = new Dialog(this);
                ackInfo.setTitle("Thanks to:");
                ackInfo.setContentView(R.layout.ack_wordfade);
                ackInfo.show();
                break;

            case R.id.instructions:
                Dialog instructions = new Dialog(this);
                instructions.setTitle("How to play:");
                instructions.setContentView(R.layout.instructions_wordfade);
                instructions.show();
                break;
            case R.id.exit:
                System.exit(0);
                break;
        }
    }

}
