package edu.neu.madcourse.mariairizarry;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;



public class Dictionary extends Activity {

    private SearchView searchView;
    private ListView wordList;
    private ImageView clearButton;
    private ArrayList<String> matched;
    private ArrayAdapter<String> matchedWords;
    private String lastEntered;
    private ProgressDialog progressDialog;
    private Context dictionaryContext;
    private ArrayList<String> exceptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary2);
        dictionaryContext = this;
        searchView = (SearchView) this.findViewById(R.id.searchView3);
        searchView.setIconified(false);
        String ex = getResources().getString(R.string.exceptions);
        exceptions = new ArrayList<String>(Arrays.asList(ex.split(" ")));

        int closeBtnId = searchView.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);

        ImageView clearBtn = (ImageView) searchView.findViewById(closeBtnId);
        wordList = (ListView) this.findViewById(R.id.listView);
        matched = new ArrayList<String>();
        matchedWords = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, matched );
       // new LoadBloom().execute();
        clearBtn.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {
                int editId = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
                EditText edit = (EditText) searchView.findViewById(editId);
                edit.setText("");
                matchedWords.clear();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String text) {
                if (text.length() >= 3) {
                    if (!matched.contains(text)) {
                        if (find(text)) {
                            matched.add(text);
                            wordList.setAdapter(matchedWords);
                            Music.playClip(dictionaryContext, R.raw.beep);
                        }
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String text) {
                return false;
            }
        });


    }

    public void showAcknowledgements (View view)
    {
        Dialog ackInfo = new Dialog(this);
        ackInfo.setTitle("Thanks to:");
        ackInfo.setContentView(R.layout.acknowledgements_page);
        ackInfo.show();
    }

    public void goBack (View view)
    {
        System.exit(0);
    }

    public boolean find(String word){

        String path = word.charAt(0) + word.length() + "";
        if(!exceptions.contains(path))
            path = word.substring(0,1) + word.length() + "";

        try{
            AssetManager am = dictionaryContext.getAssets();
            InputStream inputStream = am.open(path + ".txt");
            InputStreamReader inputReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputReader);
            String potential = null;
            while((potential = bufferedReader.readLine())!=null)
            {
                int compare = word.compareTo(potential);
                if(compare == 0) return true;
                else if(compare < 0){
                    break;
                }
            }
            bufferedReader.close();
        } catch (IOException e){

        }
        return false;
    }

}


