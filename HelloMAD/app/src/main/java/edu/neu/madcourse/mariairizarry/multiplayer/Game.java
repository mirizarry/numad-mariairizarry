package edu.neu.madcourse.mariairizarry.multiplayer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.mariairizarry.InternalStorage;
import edu.neu.madcourse.mariairizarry.R;
import edu.neu.madcourse.mariairizarry.communication.CommunicationConstants;
import edu.neu.madcourse.mariairizarry.communication.GcmNotification;
import edu.neu.madcourse.mariairizarry.wordgame.Gameplay;
import edu.neu.madcourse.mariairizarry.wordgame.Sound;
import edu.neu.madcourse.mariairizarry.wordgame.Tile;

public class Game extends Gameplay implements SensorEventListener {

    float SHUFFLE_COUNTER = 5.0f;
    public static boolean running = false;
    public float shuffleCounter;

    public boolean timeToShuffle = false;
    GoogleCloudMessaging gcm;
    Context context;
    String regid, username, partner, partnerid;
    ArrayList<String> playerList;
    HashMap<String, String> playerHash;
    ProgressDialog progressDialog, looking, networkDialog;
    String ACTION = "edu.neu.madcourse.mariairizarry.2";
    String TAG = "status";

    MultiplayerView multiplayerView;

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String USERNAME = "username";
    private TextView message;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    boolean requestDone = false;
    private int increased = 0;
    String status;
    private long shuffleMillis;
    SensorManager sensorManager;
    private long lastUpdate;
    String gameStatus;

    protected boolean[][] opponentGrid = new boolean[6][12];

    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {

            if(!pause)
            {
                multiplayerView.invalidate();
                if(handleTick())
                {
                    sendMessage("WordFade", "Move Made", username + " has made a move", "newMove");
                    increased = 0;
                }
                if(timeToShuffle)
                {
                    shuffleCounter = shuffleCounter - 0.1f;
                    if(shuffleCounter <= 0){
                        timeToShuffle = false;
                        increased = 0;
                    }
                }
                multiplayerView.invalidate();
            }
            timerHandler.postDelayed(this, 100);
        }
    };


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            status = "";
            if(extras != null){
                status = extras.getString("status2");
                if(status.equals("ready")){
                        inGame = true;
                        updatePlayerStatus();
                        looking.dismiss();
                        Sound.play(context, R.raw.wordmusic);
                }
                else if(status.equals("newMove"))
                {
                    String gridInfo = extras.getString("gridInfo");
                    updateOpponentGrid(gridInfo);
                }

                else if(status.equals("shuffle")){
                    shuffle(true);
                    String gridInfo = extras.getString("gridInfo");
                    Log.d("GRID INFO", gridInfo.toString());
//                    updateOpponentGrid(gridInfo);
                }

                else if(status.equals("winner")){
                    winner();
                }
            }
        }
    };

    public void shuffle(boolean penalty){
        int unused = unusedTiles.size();
        unusedTiles.clear();
        for(int i = 0; i < unused; i++){
            createTile();
        }

        if(penalty){
            for(int i = 0; i < placedTiles.size(); i++){
                Tile tile = placedTiles.get(i);
                if(tile !=null && tile.isInAWord())
                    tile.decrementTick(1000);
            }
        }
        Log.d("SHUFFLE", "has happened");

    }
    public void updateOpponentGrid(String info){

        int count = 0;
        multiplayerView.invalidate();
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[i].length; j++){
                char c = info.charAt(count);
                opponentGrid[i][j] = (c == '1');
                count++;
            }
        }
        multiplayerView.invalidate();
    }

    public boolean getOpponentTile(int x, int y){
        return opponentGrid[x][y];
    }
    public void reload(){
        super.reload("multi");
        opponentGrid = (boolean[][])InternalStorage.readObject(this, "opponentGrid");
    }

    public void saveGame(){
        super.saveGame("multi");
        InternalStorage.writeObject(this, "opponentGrid", opponentGrid);
    }
    protected void onResume(){
        super.onResume();
        running = true;
        registerReceiver(broadcastReceiver, new IntentFilter(ACTION));
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause()
    {
        super.onPause();
        running = false;
        unregisterReceiver(broadcastReceiver);
        saveGame();
        sensorManager.unregisterListener(this);
    }

    protected void onDestroy(){
        super.onDestroy();
        running = false;
        registerReceiver(broadcastReceiver, new IntentFilter(ACTION));
        unregisterReceiver(broadcastReceiver);
        saveGame();
        sensorManager.unregisterListener(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Sound.stop(this);
        setContentView(R.layout.activity_game_twoplayer);
        multiplayerView = new MultiplayerView(this);
        registerReceiver(broadcastReceiver, new IntentFilter(ACTION));
        gcm = GoogleCloudMessaging.getInstance(this);
        context = Game.this;
        running = true;
        partner = "";
        partnerid = "";
        status = "";
        username = "";
        regid = "";
//
//        Parse.enableLocalDatastore(this);
//        Parse.initialize(this, "G5AlZ9cl0EWVT5rxrj3SZgpIUzphskKaFzHz4Juj", "0eVIaTPE73RumH6VVJx9BfGJy6siC7HkuTeN4iHS");


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        gameStatus = extras.getString("status");
        if(extras.getString("status2")!=null)
        {
            status = extras.getString("status2");
            partner = extras.getString("partner");
            Log.d("PARTNER IS: ", partner);
            partnerid = extras.getString("partnerid");
        }
        Log.d(TAG, "status extra = " + status);

        if(checkPlayServices()){
            checkForRegistrationId();
        }
        if(gameStatus.equals("resume")) {
            setContentView(multiplayerView);
            reload();
        }
        if(status.equals("new")){
            inGame = true;
            updatePlayerStatus();
            setTiles(extras.getString("tiles"));
            //extras.putString("status", "newMultiplayer");
            setContentView(multiplayerView);
            sendMessage("", "", "", "ready");
            Sound.play(context, R.raw.wordmusic);
            Log.d("SENT: ", regid);
        }
        else if (status.equals("newMove")){
            setContentView(multiplayerView);
            reload();
            String gridInfo = extras.getString("gridInfo");
            updateOpponentGrid(gridInfo);
        }

        else if (status.equals("shuffle")){
            setContentView(multiplayerView);
            reload();
            String gridInfo = extras.getString("gridInfo");
            updateOpponentGrid(gridInfo);
            shuffle(false);
        }

        else if (status.equals("winner")){
            reload();
            setContentView(multiplayerView);
            Sound.stop(this);
            winner();
        }

        if(!isNetworkAvailable())
        {
            networkDialog = ProgressDialog.show(Game.this, "Network is unavailable",
                    "Please check your connection settings", false, false);
            while(!isNetworkAvailable());
            networkDialog.dismiss();
        }
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        lastUpdate = SystemClock.uptimeMillis();
        timerHandler.post(timerRunnable);
        gameActivity = this;

    }
    private void setTiles(String tiles)
    {
        createTiles(tiles);
    }

    public boolean addTileToRack()
    {
        boolean deducted = false;
        int remainingTiles = unusedTiles.size();
        if(remainingTiles==0)
            addPoints(15);
        else if(remainingTiles==1)
            addPoints(10);
        else if (remainingTiles==2)
            addPoints(8);
        else if (remainingTiles==3)
            addPoints(6);
        else if (remainingTiles==4)
            addPoints(4);
        else if (remainingTiles <= 10){
            addPoints(0);
            deducted = true;
        }
        else {
            deductPoints(5);
            deducted = true;
        }

        createTile();
        tileCount++;

        if(deducted){
            increased = 0;
            if(score < 0) gameOver();
        }
        else{
            increased++;
            checkForShuffle();
        }
        return deducted;
    }

    public boolean dumpTile(int x, int y)
    {
        Tile tileToDump = getPieceByCoord(x, y);
        if(isInRack(tileToDump)) {
            if (tileToDump != null) {
                deductPoints(5);
                unusedTiles.remove(tileToDump);
                for (int i = 0; i < 3; i++) {
                    Tile newTile = createTile();
                    deductPoints(newTile.getPoints() * 2);
                }
            }
        }
        else
            Toast.makeText(this, "Invalid tile!", Toast.LENGTH_LONG).show();
        increased = 0;
        return true;
    }

    public void checkForShuffle(){
        if(increased==3){
            timeToShuffle = true;
            shuffleCounter = SHUFFLE_COUNTER;
            shuffleMillis = SystemClock.uptimeMillis();
        }
    }

    public void pause()
    {
        setContentView(R.layout.activity_gameplay);
        pause = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(Game.this);

        builder.setTitle("WordFade has paused");
        builder.setMessage("What would you like to do?");
        builder.setPositiveButton("Resume", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pause = false;
                setContentView(multiplayerView);
            }

        });
        builder.setNegativeButton("Exit", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveGame();
                sensorManager.unregisterListener(Game.this);
                System.exit(0);
            }
        });
        builder.setCancelable(false);


        builder.show();
    }

    int[] scores = new int[11];
    String[] players = new String[11];
    public void updateScoreList()
    {


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Highscores");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    int count = 1;
                    for(ParseObject score : scoreList)
                    {
                       for(int i = 1; i < 11; i++)
                       {
                           if(score.getInt("rank")== i){
                              if(score.getInt("score")==scores[i] && score.getString("playerName").equals(players[i]))
                              {

                              }
                              else{
                                  score.put("score", scores[i]);
                                  score.put("playerName", players[i]);
                                  score.saveInBackground();
                              }
                           }

                       }

                    }
                    for(int i = 10; i > 0; i--)
                    {
                        if(score > scores[i])
                        {
                            rearrangeRankings(i);
                            scores[i] = score;
                            players[i] = username;
                        }
                    }
                } else {
                }
            }
        });
    }
    public void rearrangeRankings(int index){

        for(int i = index; i < 11; i++)
        {
            if(i < 10)
            {
                scores[i+1] = scores[i];
                players[i+1] = players[i];
            }
        }
    }

    public void winner(){
        inGame = false;
        updatePlayerStatus();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Highscores");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                   for(ParseObject score : scoreList)
                   {
                       int index = score.getInt("rank");

                       scores[index] = score.getInt("score");
                       players[index] = score.getString("playerName");

                   }
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
        updateScoreList();
        AlertDialog.Builder builder = new AlertDialog.Builder(Game.this);
        pause = true;
        builder.setTitle("       You Won!");
        builder.setMessage("Your final score was: " + score);
        builder.setPositiveButton("New Game", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent newGameIntent = new Intent(context, Game.class);
                newGameIntent.putExtra("status", "newGame");
                startActivity(newGameIntent);
                InternalStorage.writeObject(Game.this, "multisavedGame", false);
                gameActivity.finish();


            }

        });
        builder.setNegativeButton("Exit", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                InternalStorage.writeObject(Game.this, "multisavedGame", false);
                System.exit(0);
            }
        });
        builder.setCancelable(false);


        builder.show();
        updateTopScores();
    }

    public void updateTopScores(){}

    public boolean deductPoints(int points)
    {
        score -= points;
        if(score < 0){
            gameOver();
            sendMessage("WordFade", "You Have won!", username + " lost", "winner");
            return true;
        }
        return false;
    }
    public void gameOver(){
        inGame = false;
        updatePlayerStatus();
        AlertDialog.Builder builder = new AlertDialog.Builder(Game.this);
        pause = true;
        running = false;
        //registerReceiver()
        //unregisterReceiver(broadcastReceiver);
        builder.setTitle("       You Lost :(");
        builder.setMessage("Your final score was: " + score);
        builder.setPositiveButton("New Game", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent newGameIntent = new Intent(context, Game.class);
                newGameIntent.putExtra("status", "newGame");
                InternalStorage.writeObject(Game.this, "multisavedGame", false);
                startActivity(newGameIntent);

                gameActivity.finish();
            }

        });
        builder.setNegativeButton("Exit", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                InternalStorage.writeObject(Game.this, "multisavedGame", false);
                System.exit(0);
            }
        });
        builder.setCancelable(false);


        builder.show();
    }
    public void exit()
    {
        setContentView(R.layout.activity_gameplay);
        pause = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(Game.this);

        builder.setTitle("Are you sure you want to leave :(?");
        builder.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveGame();
                sensorManager.unregisterListener(Game.this);
                System.exit(0);
            }

        });
        builder.setNegativeButton("No", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pause = false;
                setContentView(multiplayerView);
            }
        });
        builder.setCancelable(false);


        builder.show();
    }

    public boolean placeTile(int oldx, int oldy, int newx, int newy){
        if(super.placeTile(oldx, oldy, newx, newy)) {
            sendMessage("WordFade", "Move Made", username + " has made a move", "newMove");
            if(wordFormed){
                increased++;
                wordFormed = false;
            }
            checkForShuffle();
        }
        return false;
    }

    public void moveUp(){
        super.moveUp();
        sendMessage("WordFade", "Move Made", username + " has made a move", "newMove");
    }

    public void moveDown(){
        super.moveDown();
        sendMessage("WordFade", "Move Made", username + " has made a move", "newMove");
    }
    public void moveLeft(){
        super.moveLeft();
        sendMessage("WordFade", "Move Made", username + " has made a move", "newMove");
    }
    public void moveRight(){
        super.moveRight();
        sendMessage("WordFade", "Move Made", username + " has made a move", "newMove");
    }


    private void chooseUsername()
    {
        AlertDialog.Builder register = new AlertDialog.Builder(context);
        register.setTitle("Register");
        register.setMessage("Please choose a username");
        register.setCancelable(false);

        final EditText input = new EditText(this);
        register.setView(input);
        register.setPositiveButton("Register", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.length() != 0) {
                    username = input.getText().toString();
                    registerInBackground();
                    if(requestDone) {
                        connectTo();
                    }
                }
            }
        });

        register.show();
    }

    Dialog available;
    private void connectTo()
    {

        playerList = new ArrayList<String>();
        playerHash = new HashMap<String, String>();

        new AsyncTask<Void, Integer, Void>() {

            @Override
            protected void onPreExecute()
            {
                progressDialog = ProgressDialog.show(Game.this, "Looking for players...",
                        "Please wait", false, false);

            }
            @Override
            protected Void doInBackground(Void... params) {
                ParseQuery<ParseObject> playerQuery = ParseQuery.getQuery("Player");
                playerQuery.whereEqualTo("inGame", false);
                playerQuery.findInBackground( new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if (e == null) {
                            for (ParseObject object : objects) {
                                if (!username.equals(object.getString("username"))) {
                                    String regid = object.getString("regid");
                                    playerList.add(object.getString("username"));
                                    playerHash.put(object.getString("username"), object.getString("regid"));

                                }

                            }
                            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, playerList);
                            String title = "Select an opponent";

                            if (listAdapter.isEmpty()) title = "No opponents found";
                            available = new Dialog(context);
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(title);

                            available.setCancelable(false);
                            builder.setCancelable(false);

                            ListView playerListView = new ListView(context);


                            playerListView.setAdapter(listAdapter);
                            builder.setView(playerListView);
                            available = builder.create();
                            progressDialog.dismiss();
                            available.show();


                            playerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    partner = playerList.get(position);
                                    partnerid = playerHash.get(partner);
                                    //  fetchNewData();
                                    setAllTiles();

                                    sendMessage("WordFade", username, "Would like to play Word Fade", "new");
                                    available.dismiss();
                                    setContentView(multiplayerView);
                                    looking = ProgressDialog.show(Game.this, "Waiting for " + partner + " to accept",
                                            "Please wait", false, false);
                                }
                            });
                        }
                    }
                });
                return null;

            }



        }.execute(null, null, null);
        //users.getInBackground("bwXzhRougU", new GetCallback<ParseObject>() {


    }

    @SuppressLint("NewApi")
    private void sendMessage(final String alertText, final String titleText, final String message, final String status) {

        Log.d("regid is", regid);
        Log.d("username is", username);
        if(isNetworkAvailable()) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    List<String> regIds = new ArrayList<String>();
                    String reg_device = partnerid;
                    int nIcon = R.drawable.ic_stat_cloud;
                    int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                    Map<String, String> msgParams;
                    msgParams = new HashMap<String, String>();
                    msgParams.put("data.activity", "Game");
                    msgParams.put("data.status2", status);
                    msgParams.put("data.username", username);
                    msgParams.put("data.regid", regid);
                    msgParams.put("data.alertText", alertText);
                    msgParams.put("data.titleText", titleText);
                    msgParams.put("data.contentText", message);
                    if (status.equals("new")) {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < unusedTiles.size(); i++) {
                            Tile t = unusedTiles.get(i);
                            sb.append(t.getLetter());
                        }
                        msgParams.put("data.tiles", sb.toString());
                        msgParams.put("data.status", "newMultiplayer");
                    } else if (status.equals("newMove")) {
                        msgParams.put("data.status", "resume");
                        StringBuilder gridInfo = new StringBuilder();
                        for (int i = 0; i < grid.length; i++) {
                            for (int j = 0; j < grid[i].length; j++) {
                                Tile t = grid[i][j];
                                if (t != null)
                                    gridInfo.append("1");
                                else
                                    gridInfo.append("0");
                            }
                        }
                        Log.d("GRID INFO", gridInfo.toString());
                        msgParams.put("data.gridInfo", gridInfo.toString());
                    } else if (status.equals("shuffle")) {
                        msgParams.put("data.status", "resume");
                        StringBuilder gridInfo = new StringBuilder();
                        for (int i = 0; i < grid.length; i++) {
                            for (int j = 0; j < grid[i].length; j++) {
                                Tile t = grid[i][j];
                                if (t != null)
                                    gridInfo.append("1");
                                else
                                    gridInfo.append("0");
                            }
                        }
                        Log.d("GRID INFO", gridInfo.toString());
                        msgParams.put("data.gridInfo", gridInfo.toString());
                    }
                    msgParams.put("data.nIcon", String.valueOf(nIcon));
                    msgParams.put("data.nType", String.valueOf(nType));
                    setSendMessageValues(message);
                    GcmNotification gcmNotification = new GcmNotification();
                    regIds.clear();
                    regIds.add(reg_device);
                    gcmNotification.sendNotification(msgParams, regIds,
                            Game.this);
                    msg = "sending information...";
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                    // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                }
            }.execute(null, null, null);
        }
        else
        {
            networkDialog = ProgressDialog.show(Game.this, "Network is unavailable",
                    "Please check your connection settings", false, false);
            while(!isNetworkAvailable());
            networkDialog.dismiss();

        }
    }

    private static void setSendMessageValues(String msg) {
        CommunicationConstants.alertText = "Message Notification";
        CommunicationConstants.titleText = "Sending Message";
        CommunicationConstants.contentText = msg;
    }
    private void registerInBackground()
    {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try{
                    if(gcm == null){
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(CommunicationConstants.GCM_SENDER_ID);
                    sendRegistrationIdToBackend();
                    storeRegistrationId(context);
                }
                catch (IOException e){ Log.d("Went in ", regid);}

                return "";

            }
        }.execute(null, null, null);
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void checkForRegistrationId() {

        new AsyncTask<Void, Integer, Void>() {

            @Override
            protected void onPreExecute()
            {
                progressDialog = ProgressDialog.show(Game.this, "Checking for registration",
                        "Please wait", false, false);

            }
            @Override
            protected Void doInBackground(Void... params) {

                final SharedPreferences prefs = getGCMPreferences(context);
                regid = prefs.getString(PROPERTY_REG_ID, "");
                username = prefs.getString(USERNAME, "");


                try {
                    if (regid.isEmpty()) {
                        //Log.i(TAG, "Registration not found.");
                        inGame = false;
                        regid = gcm.register(CommunicationConstants.GCM_SENDER_ID);
                        getRegidFromParse();
                    }
                    else requestDone = true;
                    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                            Integer.MIN_VALUE);
                    //Log.i(TAG, String.valueOf(registeredVersion));
                    int currentVersion = getAppVersion(context);
                    if (registeredVersion != currentVersion) {
                        //    Log.i(TAG, "App version changed.");

                    }
                    while(!requestDone);
                } catch (IOException e){}
                return null;

            }

            @Override
            protected void onPostExecute(Void result){

                progressDialog.dismiss();
                if(username.isEmpty()) chooseUsername();
                else if(status.isEmpty() && !gameStatus.equals("resume")){
                    connectTo();
                }
            }
        }.execute(null, null, null);
    }

    private String getRegidFromParse(){

        ParseQuery<ParseObject> playerQuery= ParseQuery.getQuery("Player");
        playerQuery.whereEqualTo("regid", regid);
        requestDone = false;
        playerQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(parseObject!= null){
                    username = parseObject.getString("username");
                    regid = parseObject.getString("regid");
                    inGame = false;
                    updatePlayerStatus();
                }
                else username = "";

                requestDone = true;
            }
        });
//        players.getInBackground(PLAYER_OBJECT_ID, new GetCallback<ParseObject>() {
//            @Override
//            public void done(ParseObject object, ParseException e) {
//                boolean userFound = false;
//                if (e == null) {
//                    for (String key : object.keySet()) {
//                        String registration = object.getString(key);
//                        if(!userFound && registration.equals(regid)) {
//                            username = key;
//                            storeRegistrationId(context);
//                            userFound = true;
//                            Log.d("USERNAME WAS FOUND!", username);
//                        }
//
//                    }
//                    if(!userFound) username = "";
//                    requestDone = true;
//                }
//
//            }
//
//
//        });
        return "";
    }

    private void sendRegistrationIdToBackend()
    {
        ParseObject player = new ParseObject("Player");
        player.put("username", username);
        player.put("regid", regid);
        player.put("inGame", false);
        player.saveInBackground();
        requestDone = true;
//        ParseQuery<ParseObject> usersTable = ParseQuery.getQuery("Players");
//       // ParseQuery<ParseObject> messagesTable = ParseQuery.getQuery("Messages");
//        usersTable.getFirstInBackground(new GetCallback<ParseObject>() {
//
//            @Override
//            public void done(ParseObject object, ParseException e) {
//
//                if (object != null) {
//                    object.put(username, regid);
//                    object.saveInBackground();
//
//                }
//                else
//                {
//                    ParseObject users = new ParseObject("Players");
//                    users.put(username, regid);
//                    users.saveInBackground();
//
//
//                }
//            }
//        });

    }

    private void storeRegistrationId(Context c)
    {
        final SharedPreferences prefs = getGCMPreferences(c);
        int appVersion = getAppVersion(c);
        //Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regid);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putString(USERNAME, username);
        editor.commit();
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Game.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
             getAccelerometer(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];

        float accelerationSquareRoot = (x * x + y * y + z * z)
                / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
        long actualTime = System.currentTimeMillis();
        if (accelerationSquareRoot >= 2) //
        {
            if (actualTime - lastUpdate < 200) {
                return;
            }
            lastUpdate = actualTime;
           // Toast.makeText(this, "Device has shaken", Toast.LENGTH_SHORT)
           //         .show();
            Log.d("UPDATE", " the millis were" + (SystemClock.uptimeMillis() - shuffleMillis) + " and the count for increased was " + increased);

            if(timeToShuffle){
                sendMessage("WordFade", "Move Made", username + " has shuffled your board!", "shuffle");
                timeToShuffle = false;
                increased = 0;
            }
//            if(increased==3 && (SystemClock.uptimeMillis() - shuffleMillis <= 30000) )
//             {
//                 Log.d("YOU HAVE SHUFFLED","");
//                    sendMessage("WordFade", "Move Made", username + " has shuffled your board!", "shuffle");
//                    increased = 0;
//                }
//             else if(increased==3){
//                    increased = 0;
//             }

        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    boolean inGame;
    private void updatePlayerStatus(){
        ParseQuery<ParseObject> playerQuery = ParseQuery.getQuery("Player");
        playerQuery.whereEqualTo("regid", regid);
        playerQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(parseObject!=null){
                    parseObject.put("inGame", inGame);
                    parseObject.saveInBackground();
                }
            }
        });
    }
}
