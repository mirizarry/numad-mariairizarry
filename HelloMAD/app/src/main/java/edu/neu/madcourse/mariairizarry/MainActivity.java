package edu.neu.madcourse.mariairizarry;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

import edu.neu.madcourse.mariairizarry.communication.Communication;
import edu.neu.madcourse.mariairizarry.finalProject.Description;
import edu.neu.madcourse.mariairizarry.finalProject.EatingBee;
import edu.neu.madcourse.mariairizarry.multiplayer.TwoPlayer;
import edu.neu.madcourse.mariairizarry.trickiestPart.Main;
import edu.neu.madcourse.mariairizarry.wordgame.WordFade;


public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void exitApp(View view)
    {
        System.exit(0);
    }

    public void showAbout (View view)
    {
        Dialog aboutInfo = new Dialog(this);
        aboutInfo.setTitle("About me");
        aboutInfo.setContentView(R.layout.about_page);
        TextView imei = (TextView) aboutInfo.findViewById(R.id.imei);
        imei.setText(getIMEI(this));
        aboutInfo.show();
    }

    public void launchDictionary (View view)
    {
        Intent dictionaryIntent = new Intent(this, Dictionary.class);
        startActivity(dictionaryIntent);
    }

    public void generateError (View view)
    {
        Dialog error = null;
        error.show();
    }

    public void playSudoku (View view)
    {
        Intent sudokuIntent = new Intent(this, Sudoku.class);
        startActivity(sudokuIntent);
    }

    public void playWordFade(View view)
    {
        Intent wordFadeIntent = new Intent(this, WordFade.class);
        startActivity(wordFadeIntent);
    }

    public void launchCommunication(View view)
    {
        Intent comIntent = new Intent(this, Communication.class);

        comIntent.putExtra("status", "new");
        startActivity(comIntent);
    }

    public void launchTwoPlayer(View view)
    {
        Intent twoPlayerIntent = new Intent(this, TwoPlayer.class);
        startActivity(twoPlayerIntent);
    }

    public void launchTrickiest(View view)
    {
        Intent trickyIntent = new Intent(this, Main.class);
        startActivity(trickyIntent);
    }

    public String getIMEI(Context context)
    {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
    }

    public void launchFinalProject(View view){
        Intent finalProject = new Intent(this, Description.class);
        startActivity(finalProject);
    }




}
