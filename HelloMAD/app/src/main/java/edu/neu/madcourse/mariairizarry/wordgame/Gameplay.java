package edu.neu.madcourse.mariairizarry.wordgame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import android.os.Handler;
import android.widget.Toast;

import edu.neu.madcourse.mariairizarry.*;

public class Gameplay extends Activity {

    protected BoardView boardView;
    protected ArrayList<Tile> unusedTiles;
    protected Tile[][] globalGrid = new Tile[100][100];
    protected Tile[][] grid = new Tile[6][12];
    protected int score, offsetX, offsetY;
    protected HashMap<Integer, Tile> placedTiles;
    protected ArrayList<Integer> ids;
    protected boolean musicStopped;
    protected Integer tileCount;
    protected int wordCount;
    protected SoundEffect letterDropped, wordFound;
    protected ArrayList<String> exceptions;
    protected long millis;
    protected boolean wordFormed = false;

    Context game;
    protected Activity gameActivity;
    protected boolean pause = false;
    protected Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {

            if(!pause)
            {
                boardView.invalidate();
                handleTick();
                boardView.invalidate();
            }
            timerHandler.postDelayed(this, 100);
        }
    };

    protected void createTiles(String tiles){
        unusedTiles = new ArrayList<Tile>();
        for(int i = 0; i < tiles.length(); i++) {
            char c = tiles.charAt(i);
            unusedTiles.add(new Tile(tileCount, c));
            tileCount++;
        }
    }

    protected Tile createTile()
    {
        Tile tile = new Tile(tileCount);
        unusedTiles.add(tile);
        tileCount++;
        return tile;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        game = this;
        boardView = new BoardView(this);
        setContentView(boardView);
        boardView.requestFocus();
        letterDropped = new SoundEffect(R.raw.letterdropped);
        wordFound = new SoundEffect(R.raw.chime);
        exceptions = new ArrayList<String>();
        String ex = getResources().getString(R.string.exceptions);
        exceptions = new ArrayList<String>(Arrays.asList(ex.split(" ")));

        Log.d("status is: ", getIntent().getExtras().getString("status"));
        if(getIntent().getExtras().getString("status").equals("newGame")) {
            wordCount = 0;
            tileCount = 0;
            placedTiles = new HashMap<>();
            ids = new ArrayList<>();
            score = 0;
            offsetX = 50;
            offsetY = 50;
            setAllTiles();
        }
        else if(getIntent().getExtras().getString("status").equals("newMultiplayer"))
        {
                wordCount = 0;
                tileCount = 0;
                placedTiles = new HashMap<>();
                ids = new ArrayList<>();
                score = 0;
                offsetX = 50;
                offsetY = 50;

        }
        else{
            reload("");

        }
        timerHandler.post(timerRunnable);
        gameActivity = this;
        musicStopped = false;

        Sound.play(this, R.raw.wordmusic);
    }

    public void reload(String s)
    {
        wordCount = (int) InternalStorage.readObject(this, s+"wordCount");
        tileCount = (Integer) InternalStorage.readObject(this, s+"tileCount");
        placedTiles = (HashMap<Integer, Tile>) InternalStorage.readObject(this, s+"placedTiles");
        ids = (ArrayList<Integer>) InternalStorage.readObject(this, "ids");
        unusedTiles = (ArrayList<Tile>) InternalStorage.readObject(this, s+"unusedTiles");
        score = (int) InternalStorage.readObject(this, s+"score");
        offsetX = (int) InternalStorage.readObject(this, s+"offsetX");
        offsetY = (int) InternalStorage.readObject(this, s+"offsetY");
        Iterator list = placedTiles.values().iterator();
        while(list.hasNext()){
            Tile tile = (Tile) list.next();
            globalGrid[tile.getX()][tile.getY()] = tile;
            updateDisplayGrid();
        }
    }



    public boolean handleTick()
    {
        boolean expired = false;
        ArrayList<Integer> removed = new ArrayList<>();
        for(int i = 0; i < ids.size(); i++)
        {
            Integer id = (Integer) ids.get(i);
            Tile tile = placedTiles.get(id);
            if(tile.isInAWord()) {
                tile.tick();
            }
            if(tile.expired())
            {
                placedTiles.remove(tile.getID());
                removed.add(tile.getID());
                deductPoints(tile.getPoints());
                wordCount--;
                if(!outOfDisplay(tile)){
                    Log.d("tile " + tile.getLetter() + " coords:", "Tile is at (" + tile.getX() + ", " + tile.getY() + ") with an offset of (" + offsetX + ", " + offsetY + ")");

                   grid[Math.abs(tile.getX() - offsetX)][Math.abs(tile.getY() - offsetY)] = null;
                }

                globalGrid[tile.getX()][tile.getY()] = null;


//                list = placedTiles.values().iterator();
                expired = true;
            }
        }

        for(Integer id : removed){
            ids.remove(id);
        }


//        if(wordCount > 0 && abandonedLetters()){
//            gameOver();
//        }
        return expired;
    }

    public boolean abandonedLetters()
    {
        Iterator list = placedTiles.values().iterator();
        while(list.hasNext()){
            Tile tile = (Tile) list.next();
            if(tile.abandoned(placedTiles))
                return true;
        }
        return false;
//        int x = tile.getX();
//        int y = tile.getY();
//
//        return (((globalGrid[x-1][y]!=null && globalGrid[x-1][y].isInAWord() && globalGrid[x-1][y].getTick() > 100)  &&
//                (globalGrid[x+1][y]!=null && globalGrid[x+1][y].isInAWord() && globalGrid[x+1][y].getTick() > 100)) ||
//                ((globalGrid[x][y-1]!=null && globalGrid[x][y-1].isInAWord() && globalGrid[x][y-1].getTick() > 100)  &&
//                        (globalGrid[x][y+1]!=null && globalGrid[x][y+1].isInAWord() && globalGrid[x][y+1].getTick() > 100)));
    }

    public boolean outOfDisplay(Tile tile)
    {
        int x = tile.getX();
        int y = tile.getY();
        return ((x < offsetX || x > offsetX + 6) && (y < offsetY || y > offsetY + 12));
    }




    @Override
    protected void onResume() {
        super.onResume();
        if(!musicStopped)
            Sound.play(this, R.raw.wordmusic);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        saveGame("");
        gameActivity.finish();

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveGame("");
        Sound.stop(this);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        saveGame("");
    }

    public void saveGame(String s){
        InternalStorage.writeObject(this, s+"savedGame", true);
        InternalStorage.writeObject(this, s+"wordCount", wordCount);
        InternalStorage.writeObject(this, s+"tileCount", tileCount);
        InternalStorage.writeObject(this, s+"placedTiles", placedTiles);
        InternalStorage.writeObject(this, s+"ids", ids);
        InternalStorage.writeObject(this, s+"unusedTiles", unusedTiles);
        InternalStorage.writeObject(this, s+"score", score);
        InternalStorage.writeObject(this, s+"offsetX", offsetX);
        InternalStorage.writeObject(this, s+"offsetY", offsetY);
    }

    public void setAllTiles() {
        unusedTiles = new ArrayList<Tile>();

        for (int i = 0; i < 21; i++) {
            unusedTiles.add(new Tile(tileCount));
            tileCount++;
        }
    }

    public String getTileLetter(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(unusedTiles.get(i).getLetter());

        return sb.toString();
    }

    public String getTileLetter(int i, int j) {

        StringBuilder sb = new StringBuilder();
        sb.append(grid[i][j].getLetter());

        return sb.toString();
    }

    public Tile getTile(int i) {
        return unusedTiles.get(i);
    }

    public Tile getTile(int i, int j) {
        return grid[i][j];
    }


    public ArrayList<Tile> getTiles() {
        return unusedTiles;
    }

    public int getScore()
    {
        return score;
    }

    public void addPoints(int points)
    {
        score += points;
    }

    public boolean deductPoints(int points)
    {
        score -= points;
        if(score < 0){
            gameOver();
            return true;
        }
        return false;
    }

    public void gameOver()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Gameplay.this);
        pause = true;
        builder.setTitle("       You Lost :(");
        builder.setMessage("Your final score was: " + score);
        builder.setPositiveButton("New Game", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               Intent newGameIntent = new Intent(game, Gameplay.class);
                newGameIntent.putExtra("status", "new");
                startActivity(newGameIntent);
                InternalStorage.writeObject(Gameplay.this, "savedGame", false);
                gameActivity.finish();
            }

        });
        builder.setNegativeButton("Exit", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                InternalStorage.writeObject(Gameplay.this, "savedGame", false);
                System.exit(0);
            }
        });
        builder.setCancelable(false);


        builder.show();

    }
    public boolean placeTile(int oldx, int oldy, int newx, int newy) {
        Tile tileToMove = getPieceByCoord(oldx, oldy);
        //Log.d("Movement: ", "Moving from: (" + oldx + ", " + oldy + ") to: (" + newx + ", " + newy + ") with letter " + tileToMove.getLetter() );
        if (tileToMove != null && isValidDestination(tileToMove, newx, newy) && !tileToMove.isInAWord()) {

            if(isInRack(tileToMove)) {
                unusedTiles.remove(tileToMove);
                placedTiles.put(tileToMove.getID(), tileToMove);
                ids.add(tileToMove.getID());
            }
            else{
                globalGrid[offsetX + oldx - 3][offsetY + oldy] = null;
                grid[oldx - 3][oldy] = null;
            }
            if(grid[newx][newy]!= null)
                grid[newx][newy].reset(placedTiles);
            tileToMove.setX(offsetX + newx);
            tileToMove.setY(offsetY + newy);
            globalGrid[offsetX + newx][offsetY + newy] = tileToMove;
            grid[newx][newy] = tileToMove;

            gridChanged(tileToMove);
            if(tileToMove.isInAWord()) {
                readjustDisplayGrid(newx, newy);
            }

           letterDropped.play(this);
            return true;
        }
        return false;
    }

    public boolean isValidDestination(Tile tile, int x, int y)
    {
        return (grid[x][y] == null || (tile.getLetter() == grid[x][y].getLetter() && grid[x][y].isInAWord()));
    }

    public void readjustDisplayGrid(int x, int y)
    {
        if(x == 0)
        {
            moveLeft();
        }

        else if(x == 5)
        {
            moveRight();
        }

        if(y == 0)
        {
            moveUp();
        }

        else if (y == 11)
        {
            moveDown();
        }

    }

    public void gridChanged(Tile tile)
    {
        ArrayList<Tile> word;
        boolean invert = true;

        word = checkLeft(tile);

        if(word.size() < 3)
        {
            word = checkRight(tile);
            if(word.size() >= 3) invert = false;
          Log.d("word is: ", "" + word);
        }
         if(word.size() < 3){
            word = checkDown(tile);
            if(word.size() >= 3) invert = false;
            Log.d("checking:", "Down");
        }
        if(word.size() < 3)
            word = checkUp(tile);

        if(word.size()>=3)
        {

            StringBuilder sb = new StringBuilder();
            ArrayList<Tile> possibleWord = new ArrayList<Tile>();

            for(int i = invert? word.size() - 1 : 0; invert? i >= 0 : i < word.size(); i = invert? i - 1 : i + 1)
            {
                sb.append(word.get(i).getLetter());
                possibleWord.add(word.get(i));
            }
            String wordString = sb.toString();
            Log.d("Trying to find: ", wordString);
            wordString = wordString.toLowerCase();

            millis = SystemClock.uptimeMillis();
            if(connectedToCentral(possibleWord) && find(wordString))
            {
                Log.d("TOOK ", SystemClock.uptimeMillis() - millis + " to find");
                for(int i = 0; i < word.size(); i++)
                {
                    tile = word.get(i);
                    tile.reset(placedTiles);
                    if(!word.get(i).isInAWord()) {
                        addPoints(tile.getPoints());
                        tile.setInWordFlag(true);

                    }
                    tile.setConnected(true);
                    tile.addWord(possibleWord);
                }
                wordCount++;
                wordFound.play(this);
                wordFormed = true;
            }
        }

    }

    public boolean connectedToCentral(ArrayList<Tile> potential){
        if(wordCount==0)
            return true;

        for(int i = 0; i < potential.size(); i++){
            if(potential.get(i).isConnected()) return true;
        }
        return false;
    }




    //ifvalidposition
    public ArrayList<Tile> checkLeft(Tile tile)
    {
        int x = tile.getX();
        int y = tile.getY();
        ArrayList<Tile> possibleWord = new ArrayList<Tile>();
        possibleWord.add(tile);

        while(x > 0)
        {
            Tile nextTile = globalGrid[x - 1][y];

            if(nextTile!=null)
            {
                Log.d("Checking Left:",""+ nextTile.getLetter());
                possibleWord.add(nextTile);

                x--;
            }
            else break;
        }

        return possibleWord;
    }

    public ArrayList<Tile> checkRight(Tile tile)
    {
        int x = tile.getX();
        int y = tile.getY();
        ArrayList<Tile> possibleWord = new ArrayList<Tile>();
        possibleWord.add(tile);

        while(x < 100)
        {
            Tile nextTile = globalGrid[x + 1][y];

            if(nextTile!=null)
            {
                Log.d("Checking Right:",""+ nextTile.getLetter());
                possibleWord.add(nextTile);

                x++;
            }
            else break;
        }

        return possibleWord;
    }

    public ArrayList<Tile> checkUp (Tile tile)
    {
        int x = tile.getX();
        int y = tile.getY();
        ArrayList<Tile> possibleWord = new ArrayList<Tile>();
        possibleWord.add(tile);

        while(y > 0)
        {
            Tile nextTile = globalGrid[x][y - 1];

            if(nextTile!=null)
            {
                Log.d("Checking Left:",""+ nextTile.getLetter());
                possibleWord.add(nextTile);

                y--;
            }
            else break;
        }

        return possibleWord;
    }

    public ArrayList<Tile> checkDown (Tile tile)
    {
        int x = tile.getX();
        int y = tile.getY();
        ArrayList<Tile> possibleWord = new ArrayList<Tile>();
        possibleWord.add(tile);

        while(y < 100)
        {
            Tile nextTile = globalGrid[x][y + 1];

            if(nextTile!=null)
            {
                Log.d("Checking Left:",""+ nextTile.getLetter());
                possibleWord.add(nextTile);

                y++;
            }
            else break;
        }

        return possibleWord;
    }


    public boolean addTileToRack()
    {
        boolean deducted = false;
        int remainingTiles = unusedTiles.size();
        if(remainingTiles==0)
            addPoints(15);
        else if(remainingTiles==1)
            addPoints(10);
        else if (remainingTiles==2)
            addPoints(8);
        else if (remainingTiles==3)
            addPoints(6);
        else if (remainingTiles==4)
            addPoints(4);
        else if (remainingTiles <= 10)
            addPoints(0);
        else {
            deductPoints(5);
            deducted = true;
        }

        unusedTiles.add(new Tile(tileCount));
        tileCount++;
        return deducted;
    }

    public boolean dumpTile(int x, int y)
    {
        Tile tileToDump = getPieceByCoord(x, y);
        if(isInRack(tileToDump)) {
            if (tileToDump != null) {
                deductPoints(5);
                unusedTiles.remove(tileToDump);
                for (int i = 0; i < 3; i++) {
                    Tile newTile = new Tile(tileCount);
                    unusedTiles.add(newTile);
                    deductPoints(newTile.getPoints() * 2);
                    tileCount++;
                }
            }
        }
        else
            Toast.makeText(this, "Invalid tile!", Toast.LENGTH_LONG).show();
        return true;
    }

    public Tile getPieceByCoord(int x, int y) {
        for (int i = 0; i < unusedTiles.size(); i++) {
            if (unusedTiles.get(i).getX() == x && unusedTiles.get(i).getY() == y)
                return unusedTiles.get(i);
        }
        if(x > 2){
            if(grid[x-3][y] != null)
                return grid[x-3][y];
        }
        return null;
    }

    public boolean containsTile(int x, int y){
        return getPieceByCoord(x, y) != null;
    }

    public boolean isInRack(Tile tile){
        return unusedTiles.contains(tile);
    }

    public void moveUp()
    {
        Log.d("moving up", "offset x: " + offsetX + " offset y: " + offsetY);
        if(offsetY > 0)
        {
            offsetY--;
            updateDisplayGrid();
        }

    }

    public void moveDown()
    {
        if(offsetY < 88)
        {
            offsetY++;
            updateDisplayGrid();
        }

    }

    public void moveLeft()
    {
        if(offsetX > 0)
        {
            offsetX--;
            updateDisplayGrid();
        }
    }

    public void moveRight()
    {
        if(offsetX < 94)
        {
            offsetX++;
            updateDisplayGrid();
        }
    }

    public void updateDisplayGrid()
    {
        for(int i = 0; i < 6; i++)
        {
            for(int j = 0; j < 12; j++)
            {
                Tile tile = globalGrid[i + offsetX][j + offsetY];

                if(tile!=null)
                {
//                    tile.setX(i);
//                    tile.setY(j);
                    grid[i][j] = tile;
                }
                else grid[i][j] = null;
            }
        }
    }
    public boolean rackPositionAvailable(int x, int y)
    {
        for(int i = 0; i < unusedTiles.size(); i++)
        {
            Tile tile = unusedTiles.get(i);
            if(tile.getX()== x && tile.getY()==y)
                return false;
        }
        return true;
    }

    public boolean canSplit()
    {
        return (!Tile.outOfTiles() && (unusedTiles.size() < 21));
    }

    public boolean canDump()
    {
        return (!Tile.outOfTiles() && (unusedTiles.size() < 19));
    }

    public void exit()
    {
        setContentView(R.layout.activity_gameplay);
        pause = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(Gameplay.this);

        builder.setTitle("Are you sure you want to leave :(?");
        builder.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveGame("");
                System.exit(0);
            }

        });
        builder.setNegativeButton("No", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pause = false;
                setContentView(boardView);
            }
        });
        builder.setCancelable(false);


        builder.show();
    }

    public void pause()
    {
        setContentView(R.layout.activity_gameplay);
        pause = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(Gameplay.this);

        builder.setTitle("WordFade has paused");
        builder.setMessage("What would you like to do?");
        builder.setPositiveButton("Resume", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pause = false;
                setContentView(boardView);
            }

        });
        builder.setNegativeButton("Exit", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveGame("");
                System.exit(0);
            }
        });
        builder.setCancelable(false);


        builder.show();
    }

    public void toggleMusic()
    {
        Log.d("Toggling", "");
        if(!musicStopped) {
            Sound.stop(this);
            musicStopped = true;
        }

        else {
            Sound.play(this, R.raw.wordmusic);
            musicStopped = false;
        }

    }

    public boolean find(String word){

            String path = word.charAt(0) + word.length() + "";
            if(!exceptions.contains(path))
                path = word.substring(0,1) + word.length() + "";
            Log.d("path is", path);

            try{
                AssetManager am = game.getAssets();
                InputStream inputStream = am.open(path + ".txt");
                InputStreamReader inputReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputReader);
                String potential = null;
                while((potential = bufferedReader.readLine())!=null)
                {
                    int compare = word.compareTo(potential);
                    Log.d("POTENTIAL IS: " , potential);
                    if(compare == 0) return true;
                    else if(compare < 0){
                        Log.d("BREAKING", "!!!!");
                        break;
                    }
                }
                bufferedReader.close();
            } catch (IOException e){

            }
            return false;
    }


}

