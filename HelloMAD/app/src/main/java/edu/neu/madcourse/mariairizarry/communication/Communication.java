package edu.neu.madcourse.mariairizarry.communication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.identity.intents.AddressConstants;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.mariairizarry.R;
import edu.neu.madcourse.mariairizarry.wordgame.Tile;

public class Communication extends Activity {

    public static boolean running;

    GoogleCloudMessaging gcm;
    Context context;
    String regid, username, partner, partnerid;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String USERNAME = "username";
    private TextView message;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    ProgressDialog progressDialog;
    ArrayList<String> userList;
    HashMap<String, String> userHash;
    Dialog available;
    EditText chat;
    CharSequence charseq;
    String status;
    String TAG = "Status: ";



    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           //Log.d(TAG, "new broadcast receiver called!");
           fetchNewData();
        }
    };

    protected void onResume(){
        super.onResume();
        running = true;
        registerReceiver(broadcastReceiver, new IntentFilter("edu.neu.madcourse.mariairizarry.2"));
    }

    protected void onPause()
    {
        super.onPause();
        running = false;
        unregisterReceiver(broadcastReceiver);
    }

    public void onBackPressed()
    {
        System.exit(0);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        status = extras.getString("status");
        setContentView(R.layout.activity_communication);
        running = true;
        gcm = GoogleCloudMessaging.getInstance(this);
        context = Communication.this;
        message = (TextView) findViewById(R.id.message);
        chat = (EditText) findViewById(R.id.editText);

        chat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charseq = s;
                ParseQuery<ParseObject> users = ParseQuery.getQuery("Messages");
                userList = new ArrayList<String>();
                userHash = new HashMap<String, String>();
                Log.d("Textchanged", "has");
                users.getInBackground("lRwPmmLpEP", new GetCallback<ParseObject>() {

                    @Override
                    public void done(ParseObject object, ParseException e) {

                        if (e == null) {
                            object.put(username, charseq.toString());
                            object.saveInBackground();
                            sendMessage(username +" has sent you a message!" , "resume");
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "G5AlZ9cl0EWVT5rxrj3SZgpIUzphskKaFzHz4Juj", "0eVIaTPE73RumH6VVJx9BfGJy6siC7HkuTeN4iHS");


        if(checkPlayServices() && status.equals("new")){
            regid = getRegistrationId(context);
            if(TextUtils.isEmpty(regid))
            {
                chooseUsername();
            }
            else
            {
               connectTo();
            }

        }
        else if(checkPlayServices() && status.equals("resume"))
        {
            partner = extras.getString("partner");
            partnerid = extras.getString("partnerid");
            fetchNewData();
        }
        else message.setText("Google Play services are not available at this time.");

        running = true;
        registerReceiver(broadcastReceiver, new IntentFilter ("edu.neu.madcourse.mariairizarry.2"));

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void chooseUsername()
    {
        AlertDialog.Builder register = new AlertDialog.Builder(context);
        register.setTitle("Register");
        register.setMessage("Please choose a username");
        register.setCancelable(false);

        final EditText input = new EditText(this);
        register.setView(input);
        register.setPositiveButton("Register", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.length() != 0) {
                    username = input.getText().toString();
                    registerInBackground();
                    connectTo();
                }
            }
        });

        register.show();
    }


    private void connectTo()
    {

        userList = new ArrayList<String>();
        userHash = new HashMap<String, String>();

        new AsyncTask<Void, Integer, Void>() {

            @Override
            protected void onPreExecute()
            {
                progressDialog = ProgressDialog.show(Communication.this, "Looking for players...",
                        "Please wait", false, false);

            }
            @Override
            protected Void doInBackground(Void... params) {
                ParseQuery<ParseObject> users = ParseQuery.getQuery("Users");
                users.getInBackground("7J9Zj54Gcq", new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {

                        if (e == null) {
                            for (String key : object.keySet()) {
                                if (!username.equals(key)) {
                                    String regid = object.getString(key);
                                    userList.add(key);
                                    userHash.put(key, regid);

                                }

                            }
                            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, userList);

                            available = new Dialog(context);
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Select who to chat with");

                            available.setCancelable(false);
                            builder.setCancelable(false);

                            ListView userListView = new ListView(context);


                            userListView.setAdapter(listAdapter);
                            builder.setView(userListView);
                            available = builder.create();
                            progressDialog.dismiss();
                            available.show();


                            userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    partner = userList.get(position);
                                    partnerid = userHash.get(partner);
                                    fetchNewData();

                                    sendMessage(username + " would like to chat", "resume");
                                    available.dismiss();
                                }
                            });

                        }

                    }


                });
                return null;

            }



        }.execute(null, null, null);
        //users.getInBackground("bwXzhRougU", new GetCallback<ParseObject>() {


    }

    private void fetchNewData()
    {
        new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    if(isNetworkAvailable()) {

                        ParseQuery<ParseObject> messages = ParseQuery.getQuery("Messages");
                        //messages.getInBackground("AhFN3AAwaf", new GetCallback<ParseObject>() {
                        messages.getInBackground("lRwPmmLpEP", new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject object, ParseException e) {

                                if (e == null) {
                                    message.setText(partner + ": " + object.getString(partner));
                                }
                            }

                        });
                    }
                    else message.setText("Connection could not be established");
                    return "";
                }}.execute(null, null, null);
    }

    private void registerInBackground()
    {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try{
                    if(gcm == null){
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(CommunicationConstants.GCM_SENDER_ID);
                   // Toast.makeText(context, regid, Toast.LENGTH_LONG).show();
                   // message.setText("regid is: " + regid);
                    Log.d("registration id: ", regid);
                    sendRegistrationIdToBackend();
                    storeRegistrationId(context,regid);
                }
                catch (IOException e){ Log.d("Went in ", regid);}

                return "";

            }
        }.execute(null, null, null);
    }

    private void sendRegistrationIdToBackend()
    {
//        ParseObject users = new ParseObject("Users");
//        users.put(username, regid);
//        users.saveInBackground();
        ParseQuery<ParseObject> usersTable = ParseQuery.getQuery("Users");
        ParseQuery<ParseObject> messagesTable = ParseQuery.getQuery("Messages");



        usersTable.getFirstInBackground(new GetCallback<ParseObject>() {

            @Override
            public void done(ParseObject object, ParseException e) {

                if (object != null) {
                    object.put(username, regid);
                    object.saveInBackground();

                }
                else
                {
                    ParseObject users = new ParseObject("Users");
                    users.put(username, regid);
                    users.saveInBackground();


                }
            }
        });

        messagesTable.getFirstInBackground(new GetCallback<ParseObject>() {

            @Override
            public void done(ParseObject object, ParseException e) {

                if (object != null) {
                    object.put(username, "");
                    object.saveInBackground();

                }
                else
                {
                    ParseObject messages = new ParseObject("Messages");
                    messages.put(username, "");
                    messages.saveInBackground();

                }
            }
        });


        //save id to Parse
    }

    private void storeRegistrationId(Context c, String regId)
    {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        //Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putString(USERNAME, username);
        editor.commit();
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            //Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        //Log.i(TAG, String.valueOf(registeredVersion));
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
        //    Log.i(TAG, "App version changed.");
            return "";
        }

        String username = prefs.getString(USERNAME, "");
        if(username.isEmpty())
        {
            return "";
        }
        else
            this.username = username;
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(Communication.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @SuppressLint("NewApi")
    private void sendMessage(final String message, final String status) {
        if (regid == null || regid.equals("")) {
            Toast.makeText(this, "You must register first", Toast.LENGTH_LONG)
                    .show();
            return;
        }
        if (message.isEmpty()) {
            Toast.makeText(this, "Empty Message", Toast.LENGTH_LONG).show();
            return;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                String reg_device = partnerid;
                int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<String, String>();
                msgParams.put("data.activity", "Communication");
                msgParams.put("data.alertText", "Notification");
                msgParams.put("data.titleText", "Notification Title");
                msgParams.put("data.contentText", message);
                msgParams.put("data.status", "resume");
                msgParams.put("data.username", username);
                msgParams.put("data.regid", regid);
                msgParams.put("data.status", status);
                msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                setSendMessageValues(message);
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        Communication.this);
                msg = "sending information...";
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    private static void setSendMessageValues(String msg) {
        CommunicationConstants.alertText = "Message Notification";
        CommunicationConstants.titleText = "Sending Message";
        CommunicationConstants.contentText = msg;
    }

    public void showAcknowledgements(View view)
    {
        Dialog dialog = new Dialog(this);
        dialog.setTitle("Thanks to:");
        dialog.setContentView(R.layout.com_ack);
        dialog.show();
    }

}
