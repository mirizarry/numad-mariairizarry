package edu.neu.madcourse.mariairizarry.finalProject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import edu.neu.madcourse.mariairizarry.R;

public class Description extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
    }

    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.start:
                Intent game = new Intent(this, EatingBee.class);
                finish();
                startActivity(game);
                break;
        }
    }

}
