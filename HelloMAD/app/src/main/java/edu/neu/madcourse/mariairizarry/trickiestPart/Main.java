package edu.neu.madcourse.mariairizarry.trickiestPart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import edu.neu.madcourse.mariairizarry.R;

public class Main extends Activity implements RecognitionListener{

    private final int REQ_CODE_SPEECH_INPUT = 100;
    protected SpeechRecognizer speechRecognizer;
    protected Intent recognizerIntent;
    protected String[] WORDS = {"apple", "school", "dog"};
    protected TextView prompt, word, progress;
    protected final String READY = "go";
    protected final String DONE = "I'm done";
    protected boolean started;
    protected String wordToSpell;
    protected int wordIndex;
    protected int correctLetters;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trickiest_part);
        prompt = (TextView) findViewById(R.id.prompt);
        word = (TextView) findViewById(R.id.word);
        progress = (TextView) findViewById(R.id.progress);
        started = false;
        wordIndex = 0;
        correctLetters = 0;

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());

        // SpeechRecognitionListener listener = new SpeechRecognitionListener();
        speechRecognizer.setRecognitionListener(this);

        prompt.setText("Say '" + READY+ "' to get a word to spell or say '" + DONE+"' to exit this app");
        speechRecognizer.startListening(recognizerIntent);

    }

    @Override
    protected void onDestroy (){
        super.onDestroy();
        if(speechRecognizer!=null)
            speechRecognizer.destroy();
    }
    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.d("STATUS", "I'm listening");
    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int error) {

        String msg;

        switch (error){
            case SpeechRecognizer.ERROR_CLIENT:
                msg = "client side error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                msg = "network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                msg = "no match";
                break;

            default:
                msg = error + "";
        }
        Log.d("STATUS", "Error " + msg);
        speechRecognizer.cancel();
        speechRecognizer.startListening(recognizerIntent);
    }

    @Override
    public void onResults(Bundle results) {
        ArrayList<String> matches = results.getStringArrayList(
                SpeechRecognizer.RESULTS_RECOGNITION);
        //prompt.setText(matches.get(0));
        speechRecognizer.stopListening();
        String match = matches.get(0);
        for(String m: matches)
            match+=" " + m;
        Log.d("INPUT", match);
//        if(!match.toLowerCase().contains(READY) && !match.toLowerCase().contains(DONE))
//        {
////            for(int i = 0; i < matches.size(); i++){
////                String m = matches.get(i);
////                if(m.length()==1){
////                    match = m;
////                    break;
////                }
////            }
//
//            if()
//        }
        if(match.toLowerCase().contains(READY) && !started)
        {
            started = true;
            prompt.setText("Spell the following word:");
            nextWord();
        }
        else if(match.toLowerCase().contains(DONE)){
            speechRecognizer.destroy();
            System.exit(0);
        }
        else if(started)
        {
            match = match.toLowerCase();
            Log.d("INPUT", match);
            checkLetter(match.split(" "));
        }

        speechRecognizer.startListening(recognizerIntent);

    }

    @Override
    public void onPartialResults(Bundle partialResults) {

    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }

    public void checkLetter(String[] matches){

            if(Matcher.match(matches, wordToSpell.charAt(correctLetters) + "")){
                correctLetters++;
                progress.setText(wordToSpell.substring(0,correctLetters));

                if (correctLetters == wordToSpell.length()) {
                    prompt.setText("Say '" + READY + "' to get a word to spell or say '" + DONE + "' to exit this app");
                    word.setText("");
                    progress.setText("");
                    started=false;
                    correctLetters=0;
                }
            }
    }
    public void nextWord(){

        wordToSpell = WORDS[wordIndex%3];
        word.setText(wordToSpell);
        wordIndex++;
    }


}
