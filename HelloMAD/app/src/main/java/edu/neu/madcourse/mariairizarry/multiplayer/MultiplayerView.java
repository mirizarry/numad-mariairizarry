package edu.neu.madcourse.mariairizarry.multiplayer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

import edu.neu.madcourse.mariairizarry.R;
import edu.neu.madcourse.mariairizarry.wordgame.BoardView;
import edu.neu.madcourse.mariairizarry.wordgame.Gameplay;
import edu.neu.madcourse.mariairizarry.wordgame.Tile;

/**
 * Created by mirizarry on 3/22/2015.
 */
public class MultiplayerView extends BoardView{

    private Game game;
    public MultiplayerView(Context context) {
        super(context);
        this.game = (Game) context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        //setId(ID);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        for(int i = 3; i < 9; i++)
        {
            for(int j = 0; j < 12; j++)
            {
                boolean tileToDraw = this.game.getOpponentTile(i-3, j);
                Paint tilePaint = new Paint();

                tilePaint.setColor(Color.GREEN);
                tilePaint.setAlpha(100);
                if(tileToDraw) {
                    canvas.drawRect((width * i) + 3, (height * j) + 3, (width * (i + 1)) - 2, (height * (j + 1)) - 2, tilePaint);
                }
            }
        }

        if(game.timeToShuffle){
            Paint rack = new Paint();
            Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
            foreground.setColor(getResources().getColor(R.color.puzzle_hilite));
            foreground.setStyle(Paint.Style.FILL);
            foreground.setTextSize(height * 0.37f);
            foreground.setTextScaleX(width / height);
            foreground.setTextAlign(Paint.Align.CENTER);
            rack.setColor(getResources().getColor(R.color.rack_background));

            Paint.FontMetrics fm = foreground.getFontMetrics();

            canvas.drawRect(width * 3, height * 10, width * 9, height * 12, rack);
            canvas.drawText("Shake device in the next " + String.format("%.2f", game.shuffleCounter) + " seconds", (width * 6) , (height * 11) , foreground );
            canvas.drawText("to shuffle your opponents board!", (width * 6), (height * 11) + height/2 , foreground);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float eventx = event.getX();
        float eventy = event.getY();
        int action = event.getAction();

        if(action != MotionEvent.ACTION_DOWN)
        {
            //touch on grid
            if(selection(event.getX(), event.getY())){
                if(selPiecePlaced)
                {
                    if(event.getX() > width * 3) {
                        selRectPlaced = true;
                        select((int) (event.getX() / width),
                                (int) (event.getY() / height), selRect);

                    }
                    else
                    {
                        select((int) (event.getX() / width),
                                (int) (event.getY() / height), selPiece);
                    }
                }
                else {
                    selPiecePlaced = true;
                    selRectPlaced = false;
                    select((int) (event.getX() / width),
                            (int) (event.getY() / height), selPiece);

                }
            }

            //touch on rack
            else {
                if (eventy < 7 * height)
                    select((int) (eventx / width),
                            (int) (eventy / height), selPiece);

                else if (splitButton(eventx, eventy) && this.game.canSplit()) {
                    invalidate();
                    this.game.addTileToRack();
                    invalidate();
                } else if (dumpButton(eventx, eventy) && this.game.canDump()) {
                    invalidate();
                    this.game.dumpTile((int) Math.ceil((selPiece.left - 1) / width), (int) Math.ceil((selPiece.top - 1) / height));
                    invalidate();
                } else if (upButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveUp();
                    invalidate();
                } else if (downButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveDown();
                    invalidate();
                } else if (rightButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveRight();
                    invalidate();
                } else if (leftButton(eventx, eventy)) {
                    invalidate();
                    this.game.moveLeft();
                    invalidate();
                } else if (pauseButton(eventx, eventy)){
                    this.game.pause();
                }
                else if(exitButton(eventx, eventy))
                {
                    this.game.exit();
                }
                else if(muteButton(eventx, eventy))
                {
                    this.game.toggleMusic();
                }

            }
        }
        return true;
    }

}
