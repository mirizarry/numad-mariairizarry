/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.mariairizarry.wordgame;

import android.content.Context;
import android.media.MediaPlayer;

import edu.neu.madcourse.mariairizarry.Prefs;

public class SoundEffect {
    private MediaPlayer mp = null;
    private int resource;
    /** Stop old song and start new one */
    SoundEffect(int r){
        this.resource = r;
    }
    public void play(Context context) {
        stop(context);
        if(Prefs.getMusic(context)){
            mp = MediaPlayer.create(context, resource);
            mp.start();

        }

    }

    /** Stop the music */
    public  void stop(Context context) {
        if (mp != null) {
            mp.stop();
            mp = null;
        }
    }
}
