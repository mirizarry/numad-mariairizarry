package edu.neu.madcourse.mariairizarry.multiplayer;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.parse.Parse;

import edu.neu.madcourse.mariairizarry.InternalStorage;
import edu.neu.madcourse.mariairizarry.R;
import edu.neu.madcourse.mariairizarry.wordgame.Gameplay;

public class TwoPlayer extends Activity{

    Button resume;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player);
        resume = (Button) findViewById(R.id.resume_game);

        resume.setVisibility(View.GONE);
        if(InternalStorage.readObject(this, "multisavedGame") != null) {
            Boolean saved = (Boolean) InternalStorage.readObject(this, "multisavedGame");
            if (saved)
                resume.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        resume.setVisibility(View.GONE);
        if(InternalStorage.readObject(this, "multisavedGame") != null) {
            Boolean saved = (Boolean) InternalStorage.readObject(this, "multisavedGame");
            if (saved)
                resume.setVisibility(View.VISIBLE);
        }
    }

    public void onClick (View view)
    {
        switch (view.getId()) {
            case R.id.new_game:
                Intent newGameIntent = new Intent(this, Game.class);
                newGameIntent.putExtra("status", "newGame");
                startActivity(newGameIntent);
                break;
            case R.id.resume_game:
                Intent resumeIntent = new Intent(this, Game.class);
                resumeIntent.putExtra("status", "resume");
                startActivity(resumeIntent);
                break;
            case R.id.top_scores:
                Intent topscoresIntent = new Intent(this, TopScores.class);
                startActivity(topscoresIntent);
                break;

            case R.id.instructions:
                Dialog instructions = new Dialog(this);
                instructions.setTitle("How to play:");
                instructions.setContentView(R.layout.instructions_twoplayer);
                instructions.show();
                break;
            case R.id.acknowledgements:
                Dialog ack = new Dialog(this);
                ack.setTitle("Thanks to:");
                ack.setContentView(R.layout.twoplayer_ack);
                ack.show();
                break;
        }
    }

}
