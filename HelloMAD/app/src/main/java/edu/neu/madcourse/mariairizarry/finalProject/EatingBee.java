package edu.neu.madcourse.mariairizarry.finalProject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import edu.neu.madcourse.mariairizarry.R;

public class EatingBee extends Activity {

    String difficulty;
    SharedPreferences pref;
    public static String LAUNCH_KEY = "launched", TUTORIAL = "tutorial";
    boolean launched;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eating_bee);

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(pref!=null){
            launched = pref.getBoolean(LAUNCH_KEY, false);
        }

        if(!launched)
            startTutorial();
    }

    public void startTutorial(){
        Intent tutorial = new Intent(this, Game.class);
        tutorial.putExtra(TUTORIAL, true);
        finish();
        startActivity(tutorial);
    }
    public void onClick(View view){

        switch (view.getId()){
            case R.id.play:

                final Intent playIntent = new Intent(this, Game.class);
                      playIntent.putExtra(TUTORIAL, false);
                new AlertDialog.Builder(this)
                        .setTitle("Select word difficulty:")
                        .setItems(R.array.grades,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialoginterface,
                                                        int i) {
                                        switch (i) {
                                            case 0:
                                                difficulty = "3";
                                                break;
                                            case 1:
                                                difficulty = "4";
                                                break;
                                            case 2:
                                                difficulty = "5";
                                                break;
                                        }
                                        playIntent.putExtra("difficulty", difficulty);
                                        startActivity(playIntent);
                                        finish();
                                    }
                                })
                        .show();


                break;
            case R.id.tutorial:
                final Intent tutorial = new Intent(this, Game.class);
                tutorial.putExtra(TUTORIAL, true);
                finish();
                startActivity(tutorial);
                break;

            case R.id.acknowledgements:
                Dialog ack = new Dialog(this);
                ack.setTitle("Thanks to:");
                ack.setContentView(R.layout.final_ack);
                ack.show();
                break;

        }
    }
}
