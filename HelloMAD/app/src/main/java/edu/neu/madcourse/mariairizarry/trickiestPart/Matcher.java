package edu.neu.madcourse.mariairizarry.trickiestPart;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by mariairizarry on 4/20/15.
 */
public class Matcher {

    private static final HashMap<String, String[]> filters;
    static {
        filters = new HashMap<String, String[]>();
        String [] similar = {"a", "hey", "k"};
        filters.put("a", similar);

        similar = new String [] {"b", "bee", "c", "p", "d"};
        filters.put("b", similar);

        similar = new String [] {"c","t", "see", "seeks"};
        filters.put("c", similar);

        similar = new String [] {"d","dee", "t"};
        filters.put("d", similar);

        similar = new String [] {"e","p", "t", "b", "he", "we", "eep", "eat", "deep", "d", "see"};
        filters.put("e", similar);

        similar = new String [] {"f","ask", "ass"};
        filters.put("f", similar);

        similar = new String [] {"g","d", "gee"};
        filters.put("g", similar);

        similar = new String [] {"h","each", "hey", "cage"};
        filters.put("h", similar);

        similar = new String [] {"i","hi", "high", "eye"};
        filters.put("i", similar);

        similar = new String [] {"j","dray", "dre", "k"};
        filters.put("j", similar);

        similar = new String [] {"k","okay", "que", "ok"};
        filters.put("k", similar);

        similar = new String [] {"l","hell", "el", "elle"};
        filters.put("l", similar);

        similar = new String [] {"m","10", "then", "n", "pen", "and"};
        filters.put("m", similar);

        similar = new String [] {"n", "10", "ten", "send", "and", "hun"};
        filters.put("n", similar);

        similar = new String [] {"o","oh", "hell", "so", "how"};
        filters.put("o", similar);

        similar = new String [] {"p","pee", "t"};
        filters.put("p", similar);

        similar = new String [] {"q","to", "coo", "cue", "too"};
        filters.put("q", similar);

        similar = new String [] {"r","are", "car"};
        filters.put("r", similar);

        similar = new String [] {"s","a","ask", "us", "f"};
        filters.put("s", similar);

        similar = new String [] {"t","p", "tea"};
        filters.put("t", similar);

        similar = new String [] {"u","you", "yoo", "new"};
        filters.put("u", similar);

        similar = new String [] {"v","free", "the", "b", "d"};
        filters.put("v", similar);

        similar = new String [] {"w","aw", "you", "double"};
        filters.put("w", similar);

        similar = new String [] {"x","axe", "ax", "ask", "ex"};
        filters.put("x", similar);

        similar = new String [] {"y","why", "my", "hi"};
        filters.put("y", similar);

        similar = new String [] {"z","the", "v", "g", "d"};
        filters.put("z", similar);

        similar = new String [] {"go", "gurl", "girl", "co", "cool", "call", "no"};
        filters.put("go", similar);

        similar = new String [] {"done", "gun" ,"dun", "none"};
        filters.put("done", similar);


    }

    public static boolean match(String [] matches, String expected){

        Set<String> set = new HashSet<String>();
        String [] filter = filters.get(expected.toLowerCase());
        boolean matched = false;

        for(String match: matches){
            set.add(match.toLowerCase());
        }

        Iterator it = set.iterator();
        first:
        while (it.hasNext()){
            String match = (String) it.next();
            for(String word: filter){
                if(match.equals(word)) {
                    matched = true;
                    break first;
                }
            }
        }

        return matched;

    }
}
