package edu.neu.madcourse.mariairizarry.finalProject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import edu.neu.madcourse.mariairizarry.R;
import edu.neu.madcourse.mariairizarry.trickiestPart.Matcher;


public class Game extends Activity implements RecognitionListener{


    private Context context;
    private final int TIMER = 5000;
    private SpeechRecognizer speechRecognizer;
    private Intent recognizerIntent;
    private TextView prompt, progress, time, warning, word, spinnerTitle;
    private final String READY = "go";
    private final String DONE = "done";
    private boolean started, timerStarted;
    private String wordToSpell;
    private int correctLetters;
    private ArrayList<String> wordsSpelled;
    private LinearLayout background;
    private boolean tutorial;
    private String expectedInput;
    private int tutorialStep;
    private ProgressDialog network_dialog;
    private MediaPlayer.OnCompletionListener onCompletion;
    private SoundEffect chime, good_job, great, plate_food, ready_for_next, slowly, take_a_bite, what_letter, remember_this, welcome, spell_it, another_bite;
    private boolean soundEffectDone;
    private ProgressBar spinner, progressBar;
    ArrayList<String> words;
    int timer = TIMER;

    Random randomGenerator;

    private Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            if(timerStarted) {
                stopRecognition();
                progressBar.setVisibility(View.VISIBLE);
                if (timer != 0) {
                    timer-=10;
                    //time.setText("" + timer);
                    progressBar.setProgress(timer);
//                    setTimerProgress(100 - ((timer/TIMER)*100));
                } else {
                    prompt.setText("Spell it!");
                    soundEffectDone = spell_it.play(context);
                    clear(word);
                    timer = TIMER;
                    timerStarted = false;
                    clear(time);
                    started = true;
                    //startRecognition();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
            timerHandler.postDelayed(this, 10);

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_eating_bee);

        context = this;
        Bundle extras = getIntent().getExtras();
        prompt = (TextView) findViewById(R.id.prompt);
        progress = (TextView) findViewById(R.id.progress);
        time = (TextView) findViewById(R.id.timer);
        warning = (TextView) findViewById(R.id.warning);
        word = (TextView) findViewById(R.id.word);
        background = (LinearLayout) findViewById(R.id.background);
        progressBar = (ProgressBar) findViewById(R.id.timerBar);
        spinner = (ProgressBar) findViewById(R.id.progressBar);
        spinnerTitle = (TextView) findViewById(R.id.spinnerTitle);

        progressBar.setMax(TIMER);
        progressBar.setVisibility(View.INVISIBLE);



        spinner.setVisibility(View.INVISIBLE);
        spinnerTitle.setVisibility(View.INVISIBLE);


        onCompletion = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                startRecognition();
                soundEffectDone = true;
                Log.d("started", "started speech listener in listener");
            }
        };


        good_job = new SoundEffect(R.raw.good_job, onCompletion);
        great = new SoundEffect(R.raw.great);
        plate_food = new SoundEffect(R.raw.plate_food, onCompletion);
        ready_for_next = new SoundEffect(R.raw.ready_for_next, onCompletion);
        remember_this = new SoundEffect(R.raw.remember_this, onCompletion);
        slowly = new SoundEffect(R.raw.slowly, onCompletion);
        take_a_bite = new SoundEffect(R.raw.take_a_bite, onCompletion);
        what_letter = new SoundEffect(R.raw.what_letter, onCompletion);
        welcome = new SoundEffect(R.raw.welcome_to_eating_bee, onCompletion);
        spell_it = new SoundEffect(R.raw.time_to_spell_it, onCompletion);
        another_bite = new SoundEffect(R.raw.take_another_bite, onCompletion);
        chime = new SoundEffect(R.raw.chime);

        tutorial = extras.getBoolean("tutorial");



        randomGenerator = new Random();
        words = new ArrayList<>();

        checkNetwork();
        clearAllTextViews();
        if(!tutorial){
            if(savedInstanceState==null) {
                loadDefaultStartup();

                //AUDIO HERE: "Take a bite of your food and say Go! when you're ready to start spelling"
                soundEffectDone = take_a_bite.play(this);
                prompt.setText("Say '" + READY + "' to start spelling!");
            }
            else
                retrieveSavedState(savedInstanceState);

            loadWords((String) extras.get("difficulty"));
        }
        else{
            if(savedInstanceState==null){
                loadDefaultStartup();
                prompt.setText("Make sure you have your plate of food and take a bite, when you're done say '" + READY + "'");
                expectedInput = READY;
                tutorialStep = 1;
                soundEffectDone = welcome.play(this);
                //AUDIO HERE: "Take a bite of your food and say Go! when you're ready"
            }
            else
                retrieveSavedState(savedInstanceState);

        }


        timerHandler.post(timerRunnable);

    }

    @Override
    public void onSaveInstanceState(Bundle savedState){
        super.onSaveInstanceState(savedState);

        savedState.putString("prompt",(String) prompt.getText());
        savedState.putString("progress", (String)progress.getText());
        savedState.putStringArrayList("wordsSpelled", wordsSpelled);
        savedState.putInt("timer", timer);
        savedState.putString("wordToSpell", wordToSpell);
        savedState.putInt("correctLetters", correctLetters);
        savedState.putBoolean("started", started);
        savedState.putBoolean("timerStarted", timerStarted);
        savedState.putBoolean("tutorial", tutorial);

        savedState.putBoolean("soundEffectDone", soundEffectDone);
        if(tutorial){
            savedState.putInt("tutorialStep", tutorialStep);
            savedState.putString("expectedInput", expectedInput);
            savedState.putString("time", (String) time.getText());
            savedState.putString("word", (String) word.getText().toString());
        }

    }

    private void retrieveSavedState(Bundle savedInstanceState){
        prompt.setText(savedInstanceState.getString("prompt"));
        wordsSpelled = savedInstanceState.getStringArrayList("wordsSpelled");
        timer = savedInstanceState.getInt("timer");
        wordToSpell = savedInstanceState.getString("wordToSpell");
        correctLetters = savedInstanceState.getInt("correctLetters");
        tutorial = savedInstanceState.getBoolean("tutorial");
        started = savedInstanceState.getBoolean("started");
        timerStarted = savedInstanceState.getBoolean("timerStarted");
        soundEffectDone = savedInstanceState.getBoolean("soundEffectDone");
        if(tutorial){
            tutorialStep = savedInstanceState.getInt("tutorialStep");
            expectedInput = savedInstanceState.getString("expectedInput");
            word.setText(savedInstanceState.getString("word"));
            time.setText(savedInstanceState.getString("time"));

        }
        if(!timerStarted) {
             if(!tutorial) {
                progress.setText(wordToSpell.substring(0, correctLetters));
                clear(time);
                clear(word);
            }
            else {
                 progress.setText(savedInstanceState.getString("progress"));

             }
            stopRecognition();
            if(soundEffectDone)
                startRecognition();
        }
        else {
            time.setText(timer + "");
            word.setText(wordToSpell);
        }
    }
    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        stopRecognition();
    }
    @Override
    protected void onDestroy (){
        super.onDestroy();
        stopRecognition();
    }

    private void loadWords(String grade){
        try{
            AssetManager am = this.getAssets();
            InputStream inputStream = am.open("grade" + grade);
            InputStreamReader inputReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputReader);
            String word = null;
            while((word = bufferedReader.readLine())!=null)
            {
                words.add(word);
                Log.d("ADDED", word);
            }
            bufferedReader.close();
        } catch (IOException e){
            Log.d("EXCEPTION", "IOEXCEPTION");
        }
    }

    private void checkLetter(String[] matches){
        if(Matcher.match(matches, wordToSpell.charAt(correctLetters) + "")){
            correctLetters++;
            progress.setText(wordToSpell.substring(0,correctLetters));
            prompt.setText(wordToSpell.length() - correctLetters + " more letter" + (wordToSpell.length() - correctLetters > 1? "s": ""));
            startBackgroundEffect(true);
            startRecognition();
            chime.play(this);
            if (correctLetters == wordToSpell.length()) {
                if(!tutorial) {
                    //AUDIO: "Ready for the next word? Say go. Finished with your food? Say done"
                    stopRecognition();
                    soundEffectDone = ready_for_next.play(this);
                    prompt.setText("Take a bite! Ready next word? Say '" + READY + "'. Finished your food? say '" + DONE + "'");
                    clear(progress);
                    started = false;
                    correctLetters = 0;
                    wordsSpelled.add(wordToSpell);
                    wordToSpell = "";
                }
                else{
                    //AUDIO: "Good job! Now say done to finish"
                    stopRecognition();
                    soundEffectDone = good_job.play(this);
                    prompt.setText("Good job! Now say '" + DONE + "' to finish");
                    tutorialStep = 7;
                    expectedInput = DONE;

                }
            }

        }
        else
            startBackgroundEffect(false);

    }

    private void startBackgroundEffect(boolean b){
        if(!b) background.setBackgroundResource(R.drawable.incorrect_transition);
        else background.setBackgroundResource(R.drawable.correct_transition);
        final TransitionDrawable transition = (TransitionDrawable) background.getBackground();
        transition.startTransition(500);
        background.postDelayed(new Runnable(){
            @Override
            public void run() {
                transition.reverseTransition(500);
            }
        }, 500);
    }

    private void nextWord(){

        int index = randomGenerator.nextInt(words.size());
        wordToSpell = words.get(index);
       // word.setText(wordToSpell);
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        //checkNetwork();
    }

    @Override
    public void onBeginningOfSpeech() {
         Log.d("STATUS", "I'm Listening");
    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {
        Log.d("STATUS", "I'm Done Listening");
        spinner.setVisibility(View.VISIBLE);
        spinnerTitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(int error) {
        String msg;
        switch (error){
            case SpeechRecognizer.ERROR_CLIENT:
                msg = "client side error";

                Log.d("STATUS", "Error " + msg);
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                msg = "network timeout";

                Log.d("STATUS", "Error " + msg);
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                msg = "no match";
                Log.d("STATUS", "Error " + msg);

                startBackgroundEffect(false);
                break;

            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                msg = "speech timeout";

                Log.d("STATUS", "Error " + msg);
                break;
            default:
                msg = error + "";
        }
        stopRecognition();
        checkNetwork();
        Log.d("STATUS", "Error " + msg);
        spinner.setVisibility(View.INVISIBLE);
        spinnerTitle.setVisibility(View.INVISIBLE);
        //warning.setText("I couldn't quite get that, please repeat");

    }

    @Override
    public void onResults(Bundle r) {
        spinner.setVisibility(View.INVISIBLE);
        spinnerTitle.setVisibility(View.INVISIBLE);
       receiveResults(r);
    }


    private void receiveResults(Bundle r)
    {
        ArrayList<String> results = r.getStringArrayList(
                SpeechRecognizer.RESULTS_RECOGNITION);
        //prompt.setText(matches.get(0));
        stopRecognition();
        String match = "";
        for(String m: results)
            match+=" " + m;
        Log.d("INPUT", match);
        if(!tutorial) {
            manageSpeechResults(match);
            //if(!timerStarted)
               // startRecognition();
        }
        else
        {
            String [] matches = match.split(" ");
            switch (tutorialStep){

                case 1:
                    if(Matcher.match(matches, expectedInput)){
                        //AUDIO: "Great! lets keep going"
                        soundEffectDone = great.play(this);
                        word.setText("GREAT!");
                        progress.setText("Let's keep going");
                        shortPause();

                        clearAllTextViews();
                        tutorialStep = 2;
                        word.setText("A");
                        //AUDIO: "What letter is this? Say it out loud"
                        soundEffectDone = what_letter.play(this);
                        progress.setText("Say this letter out loud");
                        expectedInput = "A";
                    }
                    else{
                        //SOUND: buzzer
                        startBackgroundEffect(false);
                        prompt.setText("Not quite, could I hear it one more time?");
                        progress.setText("Say '" + READY + "' to continue");
                        startRecognition();
                    }
                    break;

                case 2:
                    if(Matcher.match(matches, expectedInput)){

                        //SOUND: positive
                        startBackgroundEffect(true);
                        clearAllTextViews();
                        prompt.setText("Time for another bite! Say '" + READY + "' when you're done");
                        expectedInput = READY;
                        //AUDIO HERE: "Good job! now ake a bite of your food and say Go! when you're done"
                        soundEffectDone = another_bite.play(this);
                        tutorialStep = 3;
                    }
                    else{
                        //SOUND: buzzer
                        startBackgroundEffect(false);
                        prompt.setText("Not quite, could I hear it one more time?");
                        word.setText("A");
                        startRecognition();
                    }
                    break;

                case 3:
                    if(Matcher.match(matches, expectedInput)){
                        soundEffectDone = great.play(this);
                        word.setText("GREAT!");
                        progress.setText("Let's keep going");
                        shortPause();

                        clearAllTextViews();
                        //AUDIO: "Spell this word slowly, wait for the sound to keep spelling"
                        soundEffectDone = slowly.play(this);
                        prompt.setText("Spell this word slowly, wait for the letter you said to appear.");
                        word.setText("ate");
                        expectedInput = "ate";
                        tutorialStep = 4;

                    }
                    else{
                        //SOUND: buzzer
                        startBackgroundEffect(false);
                        prompt.setText("Not quite, could I hear it one more time?");
                        progress.setText("Say '" + READY + "' to continue");
                        startRecognition();
                    }
                    break;

                case 4:
                    if(Matcher.match(matches, expectedInput.charAt(correctLetters) + "")){
                        clear(progress);
                        correctLetters++;
                        SpannableString expected = new SpannableString(expectedInput);
                        expected.setSpan(new ForegroundColorSpan(Color.GREEN), 0, correctLetters, 0);
                        word.setText(expected, TextView.BufferType.SPANNABLE);
                        prompt.setText(expectedInput.length() - correctLetters + " more letters");
                        startBackgroundEffect(true);
                        chime.play(this);

                        //SOUND: positive
                        if (correctLetters == expectedInput.length()) {
                            correctLetters=0;
                            //AUDIO HERE: "Good job! now take a bite of your food and say Go! when you're done"
                            soundEffectDone = another_bite.play(this);
                            progress.setText("Good Job! Take a another bite and say '" + READY + "' when you're done");
                            expectedInput = READY;
                            tutorialStep = 5;
                        }
                        if(correctLetters > 0)startRecognition();
                    }
                    else {
                        startBackgroundEffect(false);
                        //SOUND: buzzer
                        progress.setText("Try again, you can do it!");
                        startRecognition();
                    }
                    break;
                case 5:
                    if(Matcher.match(matches, expectedInput)){
                        //SOUND: positive
                        //AUDIO: "Remember this word"
                        soundEffectDone = remember_this.play(this);
                        clearAllTextViews();
                        prompt.setText("Remember this word");
                        word.setText("tea");
                        timerStarted = true;
                        expectedInput = "tea";
                        tutorialStep = 6;
                    }
                    else
                    {
                        //SOUND: buzzer
                        startBackgroundEffect(false);
                        prompt.setText("Not quite, could I hear it one more time?");
                        progress.setText("Say '" + READY + "' to continue");
                        startRecognition();
                    }
                    break;

                case 6:
                    wordToSpell = expectedInput;
                    checkLetter(matches);
                    break;
                case 7:
                    if(Matcher.match(matches, expectedInput)){
                        startBackgroundEffect(true);
                        newGameDialog("You finished the tutorial! Want to play? Choose a grade:");

                        clearAllTextViews();
                        stopRecognition();
                        return;
                    }
                    else
                    {
                        startBackgroundEffect(false);
                        progress.setText("Try again :(");
                        startRecognition();
                    }
            }
        }


    }
    private void shortPause(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void manageSpeechResults(String match){
        if (Matcher.match(match.split(" "), READY) && !started) {
            started = true;
            nextWord();
            //AUDIO: "Remember this word"
            soundEffectDone = remember_this.play(this);
            prompt.setText("Remember this word:");
            word.setText(wordToSpell);
            timerStarted = true;
        } else if (Matcher.match(match.split(" "), DONE)) {
           // speechRecognizer.destroy();
            // System.exit(0);
            clear(prompt);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton("Quit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent mainMenu = new Intent(context, EatingBee.class);
                    finish();
                    startActivity(mainMenu);
                }
            });
            if(wordsSpelled.size()> 0)
                builder.setTitle("You spelled " + wordsSpelled.size() + " word" + (wordsSpelled.size() > 1? "s": "") +  " this meal!");
            else
                builder.setTitle("You spelled no words this meal :(");
            ListView modeList = new ListView(this);

            String[] stringArray = new String[wordsSpelled.size()];
            for (int i = 0; i < wordsSpelled.size(); i++)
                stringArray[i] = wordsSpelled.get(i);
            ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
            modeList.setAdapter(modeAdapter);

            builder.setView(modeList);
            final Dialog dialog = builder.create();

            dialog.show();
        }
        else if (started) {
            match = match.toLowerCase();
            Log.d("INPUT", match);
            checkLetter(match.split(" "));
        }
    }
    @Override
    public void onPartialResults(Bundle r) {
        receiveResults(r);
    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }


    private void loadDefaultStartup(){
        wordsSpelled = new ArrayList<>();
        started = false;
        correctLetters = 0;
        timerStarted = false;
    }

    private void clear(TextView v){
        v.setText("");
    }

    private void clearAllTextViews(){
        clear(prompt);
        clear(progress);
        clear(time);
        clear(warning);
        clear(word);

    }

    String difficulty;
    private void newGameDialog(String title){
        final Intent playIntent = new Intent(this, Game.class);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(EatingBee.LAUNCH_KEY, true);
        editor.commit();
        playIntent.putExtra(EatingBee.TUTORIAL, false);
        tutorial = false;
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setItems(R.array.grades,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface,
                                                int i) {
                                switch (i) {
                                    case 0:
                                        difficulty = "3";
                                        break;
                                    case 1:
                                        difficulty = "4";
                                        break;
                                    case 2:
                                        difficulty = "5";
                                        break;
                                }
                                playIntent.putExtra("difficulty", difficulty);
                                startActivity(playIntent);
                                finish();
                            }
                        })
                .setNegativeButton("Main Menu", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent menuIntent = new Intent(context, EatingBee.class);
                        finish();
                        startActivity(menuIntent);
                    }
                })
                .setCancelable(false)
                .show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void checkNetwork(){
        if(!isNetworkAvailable()){

            new AsyncTask<String, Void, Boolean>() {
                @Override
                protected void onPreExecute(){
                    network_dialog = new ProgressDialog(context);
                    network_dialog.setMessage("Trying to connect to speech analyzer, check your internet connection");
                    network_dialog.setCancelable(false);
                    network_dialog.show();
                }
                protected Boolean doInBackground(String... params) {
                    while(!isNetworkAvailable())
                        Log.d("stuck", "checking");
                    return false;
                }
                @Override
                protected void onPostExecute(final Boolean success){
                    network_dialog.dismiss();
                    if(soundEffectDone)
                        startRecognition();
                }
            }.execute();
        }
        else{
            if(soundEffectDone)
                startRecognition();
        }
    }

    private void stopRecognition(){
        if(speechRecognizer!=null) {
            speechRecognizer.stopListening();
            speechRecognizer.cancel();
            speechRecognizer.destroy();
            speechRecognizer = null;
        }
        Log.d("STOPPED", "with case " + tutorialStep);

    }

    private void startRecognition(){
        if(speechRecognizer==null) {
            speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
            recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                    this.getPackageName());
            speechRecognizer.setRecognitionListener(this);

            speechRecognizer.startListening(recognizerIntent);
        }
    }

//    private void setTimerProgress( int i)
//    {
//        Bitmap b = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(b);
//        Paint paint = new Paint();
//
//        paint.setColor(Color.parseColor("#c4c4c4"));
//        paint.setStrokeWidth(10);
//        paint.setStyle(Paint.Style.STROKE);
//        canvas.drawCircle(150, 150, 140, paint);
//        paint.setColor(Color.parseColor("#FFDB4C"));
//        paint.setStrokeWidth(10);
//        paint.setStyle(Paint.Style.FILL);
//        final RectF oval = new RectF();
//        paint.setStyle(Paint.Style.STROKE);
//        oval.set(10,10,290,290);
//        canvas.drawArc(oval, 270, ((90*360)/100), false, paint);
//        paint.setStrokeWidth(0);
//        paint.setTextAlign(Paint.Align.CENTER);
//        paint.setColor(Color.parseColor("#8E8E93"));
//        paint.setTextSize(140);
//        canvas.drawText(""+i, 150, 150+(paint.getTextSize()/3), paint);
//        progressBar.setImageBitmap(b);
//    }

}
