package edu.neu.madcourse.mariairizarry;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import edu.neu.madcourse.mariairizarry.communication.Communication;
import edu.neu.madcourse.mariairizarry.communication.CommunicationConstants;
import edu.neu.madcourse.mariairizarry.multiplayer.Game;

/**
 * Created by mariairizarry on 3/12/15.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    static final String TAG = "GCM_Communication";

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d("content: ", CommunicationConstants.contentText);


        Bundle extras = intent.getExtras();
        Log.d(String.valueOf(extras.size()), extras.toString());
//        String alertText = CommunicationConstants.alertText;
//        String titleText = CommunicationConstants.titleText;
//        String contentText = CommunicationConstants.contentText;

        String activity = (String) extras.get("activity");
        String alertText = (String) extras.get("alertText");
        String titleText = (String) extras.get("titleText");
        String contentText = (String) extras.get("contentText");
        String gameStatus = extras.getString("status2");

        if (!extras.isEmpty()) {

            if(activity.equals("Communication")) {

                if (Communication.running) {
                    Intent newIntent = new Intent();
                    newIntent.setAction("edu.neu.madcourse.mariairizarry.2");
                    newIntent.putExtras(intent.getExtras());
                    sendBroadcast(newIntent);
                }
                else {
                    Intent i = new Intent(this, Communication.class);
                    i.putExtra("status", extras.getString("status"));
                    i.putExtra("partnerid", extras.getString("regid"));
                    i.putExtra("partner", extras.getString("username"));
                    PendingIntent p = PendingIntent.getActivity(this, 0, i,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    sendNotification(i, p,  alertText, titleText, contentText);
                }
            }

            else if(activity.equals("Game")) {

                if(Game.running){
                    Intent newIntent = new Intent();
                    newIntent.setAction("edu.neu.madcourse.mariairizarry.2");
                    newIntent.putExtras(intent.getExtras());
                    newIntent.putExtra("status2", gameStatus);
                    newIntent.putExtra("gridInfo", extras.getString("gridInfo"));
                    sendBroadcast(newIntent);
                }

                else {
                    String status = extras.getString("status2");
                    String username = extras.getString("username");
                    String regid = extras.getString("regid");
                    String tiles = extras.getString("tiles");
                    Intent i = new Intent(this, Game.class);
                    i.putExtra("status", extras.getString("status"));
                    i.putExtra("status2", status);
                    i.putExtra("partner", username);
                    i.putExtra("partnerid", regid);
                    i.putExtra("tiles", tiles);
                    i.putExtra("gridInfo", extras.getString("gridInfo"));
                    PendingIntent p = PendingIntent.getActivity(this, 0, i,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    sendNotification(i, p, alertText, titleText, contentText);
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    public void sendNotification(Intent i, PendingIntent p, String alertText, String titleText,
                                 String contentText) {
        mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent;
        notificationIntent = i;
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.putExtra("show_response", "show_response");
        PendingIntent intent = p;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setSmallIcon(R.drawable.ic_stat_cloud)
                .setContentTitle(titleText)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(contentText))
                .setContentText(contentText).setTicker(alertText)
                .setAutoCancel(true);
        mBuilder.setContentIntent(intent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
