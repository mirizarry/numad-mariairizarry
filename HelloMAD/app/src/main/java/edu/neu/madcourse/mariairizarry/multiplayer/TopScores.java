package edu.neu.madcourse.mariairizarry.multiplayer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import edu.neu.madcourse.mariairizarry.R;

public class TopScores extends Activity {

    boolean done = false;
    TextView [] topScores = new TextView[11];
    int [] scores = new int[11];
    Date[] dates = new Date[11];
    String[] usernames = new String[11];
    TextView text1, text2, text3, text4, text5, text6, text7, text8, text9,text10;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_scores);
        context = TopScores.this;

        for( int i = 1; i < topScores.length; i++){
            topScores[i] = (TextView) findViewById(this.getResources().getIdentifier("rank_" + i, "id", this.getPackageName()));
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Highscores");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
//                    int count = 1;
                    Log.d("scorelist size is", scoreList.size() + "");
                    for(ParseObject score : scoreList)
                    {
                        int index = score.getInt("rank");
                        scores[index] = score.getInt("score");

                        usernames[index] = score.getString("playerName");
                        dates[index] = score.getUpdatedAt();
                    }

                    updateView(context);
                } else {
                }
            }
        });

    }
    TextView text;
    public void updateView(Context context)
    {
        for(int i = 1; i < topScores.length; i++){
            if(usernames[i].length() < 12){
                usernames[i] = usernames[i] + repeat(" ", (usernames[i].length() > 0? 16 - usernames.length : 13));
            }
            else usernames[i] = usernames[i].substring(0, 12);
            String spaces = "";
            if(i < 10){
               spaces = repeat(" ", 8);
            }
            else spaces = repeat(" ", 6);

            topScores[i].setText(i + spaces + repeat(" ", 7) + usernames[i] + repeat(" ", 7)+scores[i]+ repeat(" ", 10) + format(dates[i]));
        }
    }

    public String format(Date d)
    {
        DateFormat df = new SimpleDateFormat("MM/dd/yy");
        return df.format(d);
    }

    public String repeat(String s, int times)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < times; i++){
            sb.append(s);
        }

        return sb.toString();
    }


}
