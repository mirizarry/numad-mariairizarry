package edu.neu.madcourse.mariairizarry;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by mirizarry on 2/8/2015.
 */
public final class InternalStorage {

    public static void writeObject(Context c, String key, Object object)
    {

        try{
            FileOutputStream fileOutputStream = c.openFileOutput(key, Context.MODE_PRIVATE);

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(object);

            objectOutputStream.close();
            fileOutputStream.close();
        }  catch (FileNotFoundException e) {
            Log.d("status", "i entered write");}
        catch(IOException e) {Log.d("status", "IO");}


    }

    public static Object readObject(Context c, String key)
    {
        Object object = null;
        try{
            FileInputStream fileInputStream = c.openFileInput(key);

            try{
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            try{
            object = objectInputStream.readObject();}
            catch (ClassNotFoundException e) {}
            ;} catch (IOException e) {}

        } catch (FileNotFoundException e) { return null;}
        return object;
    }
}
